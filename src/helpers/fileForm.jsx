import React from 'react'
import Form from './form'
import uuid from 'uuid/v4'

const defaultState = {
  ...Form.defaultState
}

class FileForm extends Form {
  state = { ...Form.defaultState }

  handleFileChange = (e, name) => {
    const { uploadFile, deleteFile } = this.props
    const { data } = this.state

    // Don't upload if the form is invalid
    const validationErrors = this.validate()
    if (!!validationErrors && validationErrors.length > 0) {
      return
    }

    // Exit early if no file
    let file = e.target.files[0]
    if (!file) return

    // Make unique file name
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1)
    file = new File([file], `${uuid()}.${extension}`, { type: file.type })

    // Upload file
    uploadFile(file).then(url => {
      // Delete previous file if necessary
      if (data[name]) {
        deleteFile(data[name])
      }

      // Update form data
      this.handleChange({
        currentTarget: {
          name: name,
          value: url,
          setCustomValidity: () => {}
        }
      })
    })
  }

  renderFileUpload = (label, name, id, color, accept, className = '') => {
    return <React.Fragment>
        <label htmlFor={id} className={`btn btn-${color} ${className}`}>{ label }</label>
        <input id={id} name={name}
               onChange={e => this.handleFileChange(e, name)}
               type='file' accept={accept}
               hidden />
    </React.Fragment>
  }
}

FileForm.defaultState = defaultState

export default FileForm
