import seoService from '../services/seo.service'
import { Component } from 'react'

class Seo extends Component {
  seo = { ...Seo.defaultSeo }
  seoStackIndex

  componentDidMount() {
    this.addSeo()
  }
  componentWillUnmount() {
    this.removeSeo()
  }

  addSeo() {
    const { state, id } = seoService.pushState()
    this.seoStackIndex = id

    state.setTitle(this.seo.title)
    for (const name of Object.keys(this.seo.metas))
      state.setMeta(name, this.seo.metas[name])
    for (const rel of Object.keys(this.seo.links))
      state.setLink(rel, this.seo.links[rel])
    for (const key of Object.keys(this.seo.ldJson))
      state.setLdJson(key, this.seo.ldJson[key])

    seoService.renderSeo()
  }
  removeSeo() {
    seoService.popState(this.seoStackIndex)
  }

  static defaultSeo = {
    title: '',
    metas: {},
    links: {},
    ldJson: {}
  }
}

export default Seo
