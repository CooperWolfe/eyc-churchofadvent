import { Component as ReactComponent } from 'react'

class Component extends ReactComponent {
  state = {}

  constructor(props) {
    super(props)

    // Climb the inheritance tree and add static defaultState properties to the component state
    let prototype = Object.getPrototypeOf(this)
    do {
      const keys = Object.keys(prototype.constructor.defaultState || {})
      for (let i = 0; i < keys.length; ++i) {
        const key = keys[i]
        if (this.state[key] === undefined)
          this.state[key] = prototype.constructor.defaultState[key]
      }
      prototype = Object.getPrototypeOf(prototype)
    } while (prototype)
  }
}

export default Component
