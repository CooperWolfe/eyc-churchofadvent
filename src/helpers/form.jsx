import React, { Component } from 'react'
import Joi from 'joi-browser'
import DateTime from 'react-datetime'

class Form extends Component {
  static defaultState = {
    data: {},
    errors: {}
  }

  errorStyle = { color: 'red' }
  schema = {}

  validate = () => {
    const opts = { abortEarly: false, allowUnknown: true }
    const result = Joi.validate(this.state.data, this.schema, opts)
    if (!result.error) return null

    const errors = result.error.details.reduce((errs, err) => ({ ...errs, [err.path[0]]: err.message }), {})

    this.setState({ errors })
    return errors
  }
  validateProperty = (key, value) => {
    const obj = { [key]: value }
    const schema = { [key]: this.schema[key] }
    if (!schema[key]) return ''
    const { error } = Joi.validate(obj, schema)
    return error && error.details[0].message
  }
  handleSubmit = e => {
    e.preventDefault()
    if (this.validate()) return
    if (this.doSubmit) this.doSubmit(e)
  }
  handleChange = e => {
    const input = e.currentTarget
    const error = this.validateProperty(input.name, input.value)
    input.setCustomValidity(error || '')

    const { data } = this.state
    data[input.name] = input.type === 'number' ? +input.value : input.value
    this.setState({ data: { ...data } })
    if (this.doChange) this.doChange(e)
  }

  renderHeader = (title, className = '') => <h2 className={className}>{ title }</h2>
  renderField = (label, name, type = 'text', className = '', labelClassName = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <label htmlFor={name} className={labelClassName}>{ label }</label>
        <input id={name} name={name} value={data[name]} onChange={this.handleChange} className={`form-control ${className}`} type={type}/>
        { errors[name] && <small style={this.errorStyle}>{ errors[name] }</small> }
      </div>
    )
  }
  renderCheckBox = (label, name, className = '', labelClassName = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <label htmlFor={name} className={labelClassName}>{ label }</label>
        <input id={name} name={name} checked={data[name]} onChange={this.handleCheckBoxChange} className={`form-control ${className}`} type='checkbox'/>
        { errors[name] && <small style={this.errorStyle}>{ errors[name] }</small> }
      </div>
    )
  }
  renderLabellessField = (placeholder, name, type = 'text', className = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <input id={name} name={name} value={data[name]} onChange={this.handleChange} className={`form-control ${className}`} type={type} placeholder={placeholder}/>
        { errors[name] && <small style={this.errorStyle}>{ errors[name] }</small> }
      </div>
    )
  }
  renderTextArea = (label, name, rows = 3, className = '', labelClassName = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <label htmlFor={name} className={labelClassName}>{ label }</label>
        <textarea name={name} id={name} value={data[name]} onChange={this.handleChange} className={`form-control ${className}`} rows={rows}/>
        { errors[name] && <small style={this.errorStyle}>{ errors[name] }</small> }
      </div>
    )
  }
  renderLabellessTextArea = (placeholder, name, rows = 3, className = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <textarea name={name} id={name} value={data[name]} onChange={this.handleChange} className={`form-control ${className}`} rows={rows} placeholder={placeholder}/>
        { errors[name] && <small style={this.errorStyle}>{ errors[name] }</small> }
      </div>
    )
  }
  renderSelect = (label, name, opts, className = '', labelClassName = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <label htmlFor={name} className={labelClassName}>{ label }</label>
        <select name={name} id={name} className={`form-control ${className}`} value={data[name]} onChange={this.handleChange}>
          { opts.enableDefault && <option>None</option> }
          { opts.options.map((opt, i) => (
            <option key={i} value={(opts.valueKey && opt[opts.valueKey]) || opt}>
              { (opts.labelKey && opt[opts.labelKey]) || (opts.valueKey && opt[opts.valueKey]) || opt }
            </option>
          )) }
        </select>
        { errors[name] && <small style={{ color: 'red' }}>{ errors[name] }</small> }
      </div>
    )
  }
  renderButton = (type, color, label, className = '', onClick = null) => {
    const clazz = `btn btn-${color} ${className}`
    return onClick
      ? (
        <button className={clazz} type={type} onClick={onClick}>{ label }</button>
      )
      : (
        <button className={clazz} type={type}>{ label }</button>
      )
  }
  renderDateTimePicker = (label, name, labelClassName = '', showTime = true, min = null, max = null) => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <label className={labelClassName}>{ label }</label>
        <DateTime value={data[name]}
                  timeFormat={showTime}
                  onChange={date => this.handleDateTimePickerChange(name, date, min, max)} />
        { errors[name] && <small style={this.errorStyle}>{ errors[name] }</small> }
      </div>
    )
  }

  handleCheckBoxChange = (e) => {
    this.handleChange({ currentTarget: {
      name: e.currentTarget.name,
      value: e.currentTarget.checked,
      setCustomValidity: () => {}
    }})
  }
  handleDateTimePickerChange = (name, date, min, max) => {
    // Convert from moment
    if (date.constructor.name === 'Moment') date = date.toDate()

    // Hold to constraints
    if ((!!min && date < min) || (!!max && date > max)) return;

    // Change value
    this.handleChange({ currentTarget: {
      name: name,
      value: date,
      setCustomValidity: () => {}
    }})
  }
}

export default Form
