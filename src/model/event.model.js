import Model from './model'
import { dayNames, monthNames, shortDayNames, toSmallTimeString } from '../utilities/date.utility'

export default class Event extends Model {
  constructor(id, title, start, end, allDay, type, description) {
    super(id)
    this.title = title
    this.start = start
    this.end = end
    this.allDay = allDay
    this.type = type
    this.description = description
  }

  update(updateObject) {
    return new Event(
      this.id,
      updateObject.title || this.title,
      updateObject.start || this.start,
      updateObject.end || this.end,
      this.coallesce(updateObject.allDay, this.allDay),
      updateObject.type || this.type,
      updateObject.description || this.description,
    )
  }

  get typeDisplay() {
    const displayValues = {
      'middle-school': 'Middle School',
      'high-school': 'High School',
      'college': 'College'
    }
    return displayValues[this.type]
  }


  title
  start
  end
  allDay
  type
  description

  static fromDocumentSnapshot(documentSnapshot) {
    const data = documentSnapshot.data()
    data.start = new Date(data.start.seconds * 1000)
    data.end = new Date(data.end.seconds * 1000)
    return Event.fromObject(data)
  }

  static fromObject(createObject) {
    return new Event(
      createObject._id,
      createObject.title,
      createObject.start,
      createObject.end,
      createObject.allDay,
      createObject.type,
      createObject.description,
    )
  }

  get dateTimeSummary() {
    if (this.start.toLocaleDateString() === this.end.toLocaleDateString()) {
      if (this.allDay) {
        return `${dayNames[this.start.getDay()]}, ${monthNames[this.start.getMonth()]} ${this.start.getDate()}`
      }
      else {
        return `${dayNames[this.start.getDay()]}, ${monthNames[this.start.getMonth()]} ${this.start.getDate()} | ${toSmallTimeString(this.start)} - ${toSmallTimeString(this.end)}`
      }
    }
    else {
      if (this.allDay) {
        return `${dayNames[this.start.getDay()]}, ${monthNames[this.start.getMonth()]} ${this.start.getDate()} - ${dayNames[this.end.getDay()]}, ${monthNames[this.end.getMonth()]} ${this.end.getDate()}`
      }
      else {
        return `${dayNames[this.start.getDay()]}, ${monthNames[this.start.getMonth()]} ${this.start.getDate()} at ${toSmallTimeString(this.start)} - ${dayNames[this.end.getDay()]} ${monthNames[this.end.getMonth()]}, ${this.end.getDate()} at ${toSmallTimeString(this.end)}`
      }
    }
  }
}
