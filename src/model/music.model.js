import Model from './model'

export default class Music extends Model {
  constructor(id, title, date, description, files) {
    super(id)
    this.title = title
    this.date = date
    this.description = description
    this.files = files
  }

  title
  date
  description
  files

  update(updateObject) {
    return new Music(
      this.id,
      updateObject.title || this.title,
      updateObject.date || this.date,
      updateObject.description || this.description,
      updateObject.files || this.files
    )
  }

  static fromDocumentSnapshot(documentSnapshot) {
    const data = documentSnapshot.data()
    data.date = new Date(data.date.seconds * 1000)
    return Music.fromObject(data)
  }

  static fromObject(createObject) {
    return new Music(
      createObject._id,
      createObject.title,
      createObject.date,
      createObject.description,
      createObject.files || []
    )
  }
}
