import YouthMinister from '../youthMinister.model'

export default class YouthMinisterFactory {
  static create() {
    return new YouthMinister(null, '', '', '', '', '', '', '')
  }
}
