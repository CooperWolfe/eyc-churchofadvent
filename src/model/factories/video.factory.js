import Video from '../video.model'

export default class VideoFactory {
  static create() {
    return new Video(null, '', new Date())
  }
}
