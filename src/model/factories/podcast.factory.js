import Podcast from '../podcast.model'

export default class PodcastFactory {
  static create() {
    return new Podcast(null, '', new Date(), '', [])
  }
}
