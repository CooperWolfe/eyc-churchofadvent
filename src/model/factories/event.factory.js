import Event from '../event.model'

export default class EventFactory {
  static create() {
    const now = new Date()
    return new Event(null, '', now, now, true, 'middle-school', '')
  }
}
