import Slide from '../slide.model'

export default class SlideFactory {
  static create() {
    return new Slide(null, '', new Date(), '', '', '')
  }
}
