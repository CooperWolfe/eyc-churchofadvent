import Form from '../form.model'

export default class FormFactory {
  static create() {
    return new Form(null, '', '')
  }
}
