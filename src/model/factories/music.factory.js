import Music from '../music.model'

export default class MusicFactory {
  static create() {
    return new Music(null, '', new Date(), '', [])
  }
}
