import Model from './model'

export default class Form extends Model {
  constructor(id, title, url) {
    super(id)
    this.title = title
    this.url = url
  }

  title
  url

  update(updateObject) {
    return new Form(
      this.id,
      updateObject.title || this.title,
      updateObject.url || this.url
    )
  }

  static fromDocumentSnapshot(documentSnapshot) {
    const data = documentSnapshot.data()
    return Form.fromObject(data)
  }

  static fromObject(createObject) {
    return new Form(
      createObject._id,
      createObject.title,
      createObject.url
    )
  }
}
