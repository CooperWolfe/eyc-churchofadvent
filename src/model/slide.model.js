import Model from './model'

export default class Slide extends Model {
  constructor(id, title, date, imageUrl, buttonText, buttonUrl) {
    super(id)
    this.title = title
    this.date = date
    this.imageUrl = imageUrl
    this.buttonText = buttonText
    this.buttonUrl = buttonUrl
  }

  title
  date
  imageUrl
  buttonText
  buttonUrl

  update(updateObject) {
    return new Slide(
      this.id,
      updateObject.title || this.title,
      updateObject.date || this.date,
      updateObject.imageUrl || this.imageUrl,
      updateObject.buttonText || this.buttonText,
      updateObject.buttonUrl || this.buttonUrl
    )
  }

  static fromDocumentSnapshot(documentSnapshot) {
    const data = documentSnapshot.data()
    data.date = new Date(data.date.seconds * 1000)
    return Slide.fromObject(data)
  }

  static fromObject(createObject) {
    return new Slide(
      createObject._id,
      createObject.title,
      createObject.date,
      createObject.imageUrl,
      createObject.buttonText,
      createObject.buttonUrl
    )
  }
}
