import Model from './model'

export default class Video extends Model {
  constructor(id, url, date) {
    super(id)
    this.url = url
    this.date = date
  }

  url
  date

  update(updateObject) {
    return new Video(
      this.id,
      updateObject.url || this.url,
      updateObject.date || this.date,
    )
  }

  static fromDocumentSnapshot(documentSnapshot) {
    const data = documentSnapshot.data()
    data.date = new Date(data.date.seconds * 1000)
    return Video.fromObject(data)
  }

  static fromObject(createObject) {
    return new Video(
      createObject._id,
      createObject.url,
      createObject.date,
    )
  }
}
