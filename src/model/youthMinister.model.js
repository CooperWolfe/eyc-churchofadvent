import Model from './model'

export default class YouthMinister extends Model {
  constructor(id, imageUrl, lastName, firstName, email, officePhone, cellPhone, bio) {
    super(id)
    this.imageUrl = imageUrl
    this.lastName = lastName
    this.firstName = firstName
    this.email = email
    this.officePhone = officePhone
    this.cellPhone = cellPhone
    this.bio = bio
  }

  imageUrl
  lastName
  firstName
  email
  officePhone
  cellPhone
  bio

  update(updateObject) {
    return new YouthMinister(
      this.id,
      updateObject.imageUrl || this.imageUrl,
      updateObject.lastName || this.lastName,
      updateObject.firstName || this.firstName,
      updateObject.email || this.email,
      updateObject.officePhone || this.officePhone,
      updateObject.cellPhone || this.cellPhone,
      updateObject.bio || this.bio
    )
  }

  static fromDocumentSnapshot(documentSnapshot) {
    const data = documentSnapshot.data()
    return YouthMinister.fromObject(data)
  }

  static fromObject(createObject) {
    return new YouthMinister(
      createObject._id,
      createObject.imageUrl,
      createObject.lastName,
      createObject.firstName,
      createObject.email,
      createObject.officePhone,
      createObject.cellPhone,
      createObject.bio
    )
  }
}
