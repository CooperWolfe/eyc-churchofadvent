import { Component } from 'react'
import { connect } from 'react-redux'
import { logout } from '../../stores/auth/auth.actions'
import history from '../../services/history.service'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
})

class Logout extends Component {
  state = {}

  componentDidMount() {
    const { logout } = this.props
    logout()
    history.push('/login')
  }

  render() {
    return null
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout)
