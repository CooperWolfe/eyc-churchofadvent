import React, { Component } from 'react'
import { connect } from 'react-redux'
import './youth-minister-contact.css'
import PropTypes from 'prop-types'
import { toPhone } from '../../utilities/string.utility'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class YouthMinisterContact extends Component {
  state = {}

  renderEmail = text => {
    return (
      <li className='list-group-item text-center'>
        <i className='float-left fa fa-envelope'/>
        <a href={`mailto:${text}@churchofadvent.org`} className='font-weight-bold c-accent'>{ text }</a>
        <br/>
        <span className=''>@churchofadvent.org</span>
      </li>
    )
  }

  renderPhone = (phone, reason) => {
    if (!phone) return null
    return (
      <li className='list-group-item text-center'>
        <i className='float-left fa fa-phone'/>
        <a href={`tel:${phone}`} className='font-weight-bold c-accent'>{ toPhone(phone) }</a>
        <br/>
        <span className=''>({ reason })</span>
      </li>
    )
  }

  render() {
    const { imageUrl, firstName, lastName, email, officePhone, cellPhone } = this.props.contact

    const name = `${firstName} ${lastName}`

    return (
      <div className='youth minister contact'>
        <div className='card'>
          <img className='card-img-top' src={imageUrl} alt={name}/>
          <div className='card-body'>
            <h5 className='card-title text-center'>
              { name }
              <br/>
              <em style={{ fontSize: '1rem' }}>Youth Minister</em>
            </h5>
            <ul className='list-group list-group-flush' style={{ marginBottom: '-.5rem' }}>
              { this.renderEmail(email) }
              { this.renderPhone(officePhone, 'Office') }
              { this.renderPhone(cellPhone, 'Cell') }
            </ul>
          </div>
        </div>
      </div>
    )
  }

  static propTypes = {
    contact: PropTypes.object.isRequired
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(YouthMinisterContact)
