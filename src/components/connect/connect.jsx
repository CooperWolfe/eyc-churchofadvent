import React, { Component } from 'react'
import { connect } from 'react-redux'
import YouthMinisterContact from './youth-minister-contact'
import './connect.css'
import WhoSnippet from '../home/whoSnippet'
import { Modules } from '../../stores/app.modules'
import { fetchYouthMinisters } from '../../stores/youth-ministers/youthMinisters.actions'
import Loader from '../shared/loader'

const mapStateToProps = state => ({
  loading: state[Modules.YOUTH_MINISTERS].loading,
  youthMinisters: state[Modules.YOUTH_MINISTERS].list
})
const mapDispatchToProps = dispatch => ({
  fetchYouthMinisters: () => dispatch(fetchYouthMinisters())
})

class Connect extends Component {
  state = {}

  componentDidMount() {
    const { fetchYouthMinisters } = this.props
    fetchYouthMinisters()
  }

  render() {
    const { youthMinisters, loading } = this.props

    return (
      <div className='connect bg-light c-primary'>
        <div className='container pt-5 pb-5'>
          <div className='row'>
            <div className='col-md-4 text-center'>
              <WhoSnippet imageSrc='/logo-purple.png'/>
              <hr/>
              <h2>Office Hours</h2>
              <h5>Mon. - Thur.</h5>
              <p>9:00 A.M. - 5:00 P.M.</p>
              <h5>Fridays</h5>
              <p>9:00 A.M. - 1:00 P.M.</p>
              <hr/>
            </div>
            <div className='col-md-8'>
              <div className='container'>
                <div className='row'>
                  { loading !== 0 && <div className='text-center m-auto h1'><Loader className='c-primary' /></div> }
                  { loading === 0 && youthMinisters.map((youthMinister, i) => (
                    <div className='col-lg-6'>
                      <YouthMinisterContact contact={youthMinister} key={i} />
                    </div>
                  )) }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Connect)
