import { connect } from 'react-redux'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { Collapse, Dropdown, DropdownMenu, DropdownToggle, NavbarToggler } from 'reactstrap'
import './navbar.css'
import { Modules } from '../stores/app.modules'

const propTypes = {
  style: PropTypes.object,
  breakpoint: PropTypes.string
}
const defaultState = {
  left: [],
  right: [],
  leftAuthenticated: [],
  rightAuthenticated: [],
  linkOpen: [],
  open: false
}

const mapStateToProps = state => ({
  authenticated: state[Modules.AUTH].authenticated
})
const mapDispatchToProps = dispatch => ({})

class Navbar extends Component {
  state = {
    ...defaultState,
    left: [
      { text: 'About', url: '/about' },
      { text: 'Connect', url: '/connect' },
      { text: 'Events', url: '/events' },
      { text: 'Media', links: [
        { text: 'Podcasts', url: '/media/podcasts' },
        { text: 'Music', url: '/media/music' },
        { text: 'Videos', url: '/media/videos' }
      ] },
      { text: 'Parent Portal', links: [
        { text: 'Forms', url: '/forms' },
        { text: 'How Can I Serve?', url: '/serve' }
      ] }
    ],
    right: [
      { text: <i style={{ fontSize: '1.5rem' }} className='fa fa-user-circle h1' />, url: '/login' }
    ],
    leftAuthenticated: [
      { text: 'Youth Ministers', url: '/admin/youth-ministers' },
      { text: 'Events', url: '/admin/events' },
      { text: 'Media', url: '/admin/media/podcasts' },
      { text: 'Forms', url: '/admin/forms' }
    ],
    rightAuthenticated: [
      { text: 'Logout', url: '/logout' }
    ],
    brandHovering: false
  }

  renderBrand = () => {
    const { brandHovering } = this.state
    return (
      <img onMouseEnter={() => this.setState({ brandHovering: true })}
           onMouseLeave={() => this.setState({ brandHovering: false })}
           src={brandHovering ? '/logo-simple-purple.png' : '/logo-simple.png'} style={{ maxHeight: '100%', maxWidth: 30 }}/>
    )
  }
  renderLinks = (links, className) => {
    className = className ? `navbar-nav ${className}` : 'navbar-nav'

    return (
      <ul className={className}>
        { links.map((link, i) => {
          if (link.links) return this.renderDropdownLink(link, i)
          if (link.url) return this.renderLink(link, i)
          return <li key={i}>Links must be of the format {'{'} text: string, [url/links]: [string/array] {'}'}</li>
        }) }
      </ul>
    )
  }
  renderDropdownLink = (link, i) => {
    const { linkOpen } = this.state

    return (
      <Dropdown nav key={i} className='m-auto' isOpen={linkOpen[i]} toggle={() => this.toggleLink(i)}>
        <DropdownToggle nav caret>{ link.text }</DropdownToggle>
        <DropdownMenu onClick={() => this.toggleLink(i)}>
          { link.links.map((link, i) => (
            <NavLink key={i} className='dropdown-item c-primary' to={link.url} exact={link.exact}>{ link.text }</NavLink>
          )) }
        </DropdownMenu>
      </Dropdown>
    )
  }
  renderLink = (link, i) => {
    return (
      <li key={i} className='nav-item m-auto'>
        <NavLink to={link.url} className='nav-link c-light' exact={link.exact}>{link.text}</NavLink>
      </li>
    )
  }
  renderLeft = () => {
    const { authenticated } = this.props
    const { left, leftAuthenticated } = this.state
    return this.renderLinks(authenticated ? leftAuthenticated : left, 'mr-auto')
  }
  renderRight = () => {
    const { authenticated } = this.props
    const { right, rightAuthenticated } = this.state
    return this.renderLinks(authenticated ? rightAuthenticated : right)
  }
  render() {
    const { style, breakpoint } = this.props
    const { open } = this.state

    let className
    switch (breakpoint) {
    case 'sm': className = 'navbar navbar-expand-sm'; break
    case 'md': className = 'navbar navbar-expand-md'; break
    case 'lg': className = 'navbar navbar-expand-lg'; break
    case 'xl': className = 'navbar navbar-expand-xl'; break
    default: className = 'navbar navbar-expand'
    }

    return (
      <nav className={'wd ' + className} style={{ ...style }}>
        <div className='container'>
          <NavLink className='navbar-brand c-light' to='/' exact>{this.renderBrand()}</NavLink>
          <NavbarToggler onClick={this.toggle}><i className='fa fa-bars' style={{ color: '' }}/></NavbarToggler>
          <Collapse isOpen={open} navbar>
            { this.renderLeft() }
            { this.renderRight() }
          </Collapse>
        </div>
      </nav>
    )
  }

  toggle = () => {
    const { open } = this.state
    this.setState({ open: !open })
  }
  toggleLink = i => {
    let { linkOpen: old } = this.state

    const linkOpen = Array(old.length)
    linkOpen.fill(false)
    linkOpen[i] = !old[i]

    this.setState({ linkOpen })
  }
}

Navbar.propTypes = propTypes
Navbar.defaultState = defaultState

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
