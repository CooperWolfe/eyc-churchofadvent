import React, { Component } from 'react'
import { connect } from 'react-redux'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class Serve extends Component {
  state = {}

  render() {
    return (
      <h1 className='text-center p-5'>Content Coming Soon!</h1>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Serve)
