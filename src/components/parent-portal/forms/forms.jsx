import React from 'react'
import { connect } from 'react-redux'
import { Modules } from '../../../stores/app.modules'
import { fetchForms } from '../../../stores/forms/forms.actions'
import FileForm from '../../../helpers/fileForm'
import { uploadFile } from '../../../stores/file/file.actions'
import logService from '../../../services/log.service'
import emailService from '../../../services/email.service'
import { fetchYouthMinisters } from '../../../stores/youth-ministers/youthMinisters.actions'
import { NavLink } from 'react-router-dom'

const mapStateToProps = state => ({
  forms: state[Modules.FORMS].list,
  youthMinisters: state[Modules.YOUTH_MINISTERS].list
})
const mapDispatchToProps = dispatch => ({
  fetchForms: () => dispatch(fetchForms()),
  fetchYouthMinisters: () => dispatch(fetchYouthMinisters()),
  uploadFile: file => dispatch(uploadFile(file, 'forms'))
})

class Forms extends FileForm {
  state = {
    ...FileForm.defaultState,
    loading: false
  }

  componentDidMount() {
    const { fetchForms, fetchYouthMinisters } = this.props
    fetchForms()
    fetchYouthMinisters()
  }

  doChange = e => {
    const { forms } = this.props
    const { data } = this.state

    const form = forms.filter(f => f._id === e.currentTarget.name)[0]
    const url = data[e.currentTarget.name]
    if (form && url) {
      emailService.sendEmail(
        'eyc@churchofadvent.org',
        'noreply@wolfewebdev.com',
        form.title,
        `${form.title} submitted from your website.\r\nPlease visit ${url} to download the form.`
      ).then(() => {
        logService.alert(`Successfully sent: ${form.title}`)
      })
    }
  }

  render() {
    const { forms } = this.props

    return (
      <div className='forms container text-center pb-4'>
        <h1 className='pt-3 pb-3'>Forms</h1>
        <div className='alert alert-light'>
          If you have any questions, feel free to <strong><NavLink className='c-accent' to='/connect'>contact</NavLink></strong> one of our Youth Ministers
        </div>
        <div className='row'>
          <div className='col-md-6 m-auto'>
            <ul className='list-group'>
              { forms.map((form, i) => (
                <li className='list-group-item' key={i}>
                  <h5 className='c-primary'>{ form.title }</h5>
                  <div className='list-group list-group-flush list-group-horizontal'>
                    <a href={form.url} className='list-group-item w-50 border-0 c-accent'>Download</a>
                    { this.renderFileUpload('Upload', form._id, form._id, '', 'application/*', 'list-group-item w-50 border-0 c-accent cursor-pointer hover-underline') }
                  </div>
                </li>
              )) }
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Forms)
