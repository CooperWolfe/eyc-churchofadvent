import React, { Component } from 'react'

class Footer extends Component {
  render() {
    const { className, style, ...props } = this.props

    return (
      <div className={`text-center p-2 c-light ${className}`} style={{ fontSize: '.67rem', ...style }} { ...props }>
        © 2019 EYC Episcopal Church of the Advent. All rights reserved.
      </div>
    )
  }
}

export default Footer
