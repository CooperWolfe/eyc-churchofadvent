import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import Podcasts from './podcasts/podcasts'
import Music from './music/music'
import Videos from './videos/videos'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class Media extends Component {
  state = {}

  render() {
    const { path } = this.props.match

    return (
      <Switch>
        <Route path={`${path}/podcasts`} component={Podcasts} />
        <Route path={`${path}/music`} component={Music} />
        <Route path={`${path}/videos`} component={Videos} />
      </Switch>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Media)
