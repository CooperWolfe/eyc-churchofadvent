import React, { Component } from 'react'
import { connect } from 'react-redux'
import VideoGallery from './video-gallery'
import { Modules } from '../../../stores/app.modules'
import { fetchVideos } from '../../../stores/videos/videos.actions'
import Loader from '../../shared/loader'

const mapStateToProps = state => ({
  loading: state[Modules.VIDEOS].loading > 0 && !state[Modules.VIDEOS].list.length,
  videos: state[Modules.VIDEOS].list.sort((lhs, rhs) => lhs.date > rhs.date)
})
const mapDispatchToProps = dispatch => ({
  fetchVideos: () => dispatch(fetchVideos())
})

class Videos extends Component {
  state = {}

  componentDidMount() {
    const { fetchVideos } = this.props
    fetchVideos()
  }

  render() {
    const { videos, loading } = this.props

    if (loading) return <Loader/>

    return (
      <div className='videos container'>
        <h1 className='text-center pt-3 pb-3'>Videos</h1>
        <VideoGallery videos={videos}/>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Videos)
