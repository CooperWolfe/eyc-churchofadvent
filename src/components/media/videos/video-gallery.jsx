import React, { Component } from 'react'
import { connect } from 'react-redux'
import ImageGallery from 'react-image-gallery'
import { getEmbedUrl, getThumbnailUrl, isYoutubeUrl } from '../../../utilities/youtube.utility'
import './video-gallery.css'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class VideoGallery extends Component {
  state = {
    showVideo: {}
  }

  static defaultProps = {
    defaultText: 'Videos coming soon!'
  }

  renderVideo = item => {
    const { onDelete } = this.props

    function extractType(url) {
      const match = url.match(/\.(\w+)\?/)
      return `video/${match ? match[1] : 'mp4'}`
    }

    return (
      <div className='image-gallery-image video-gallery text-center position-relative' style={{ maxHeight: 500 }}>
        { onDelete && (
          <div className='delete' onDoubleClick={() => onDelete(item.video.id)}>
            <i className='fa fa-trash'><br/>(double click)</i>
          </div>
        ) }
        <div className='embed-responsive embed-responsive-4by3 d-inline-block' style={{ maxHeight: 500 }}>
          {
            isYoutubeUrl(item.embedUrl) ? (
              <iframe title='Meet the youth ministers'
                      src={item.embedUrl}
                      allow='accelerometer; encrypted-media; gyroscope; picture-in-picture'
                      className='embed-responsive-item'
                      allowFullScreen/>
            ) : (
              <video controls className='embed-responsive-item' width='100%' height='100%'>
                <source src={item.embedUrl} type={extractType(item.embedUrl)}/>
              </video>
            )
          }
        </div>
      </div>
    )
  }
  render() {
    const { videos, defaultText } = this.props

    if (!videos.length) {
      return <h2 className='text-center'>{ defaultText }</h2>
    }

    const items = videos.map(video => ({
      embedUrl: getEmbedUrl(video.url) || video.url,
      original: getThumbnailUrl(video.url, 0),
      thumbnail: getThumbnailUrl(video.url),
      renderItem: this.renderVideo,
      video
    }))

    return (
      <ImageGallery items={items} />
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoGallery)
