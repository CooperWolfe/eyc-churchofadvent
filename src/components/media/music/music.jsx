import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modules } from '../../../stores/app.modules'
import { fetchMusic } from '../../../stores/music/music.actions'
import MusicCard from './music-card'
import { Route, Switch } from 'react-router-dom'

const mapStateToProps = state => ({
  music: state[Modules.MUSIC].list
})
const mapDispatchToProps = dispatch => ({
  fetchMusic: () => dispatch(fetchMusic())
})

class Music extends Component {
  state = {}

  componentDidMount() {
    const { fetchMusic } = this.props
    fetchMusic()
  }

  render() {
    const { music, match: { path }, location: { search } } = this.props

    const match = search.match(/\?id=([a-f0-9-]+)/)
    const id = match && match[1]
    const item = music.filter(p => p.id === id)[0]

    return (
      <Switch>
        <Route path={`${path}/:title`}>{ item && (
          <div className='music text-center'>
            { item && <MusicCard music={item} /> }
          </div>
        ) }</Route>
        <Route path={path}>
          <div className='music container'>
            <h1 className='text-center pt-3'>Music</h1>
            <div className='row'>
              { music.length
                ? music.map((item, i) => (
                  <div key={i} className='col-12 col-sm-6 col-lg-4 mt-3'>
                    <MusicCard music={item}/>
                  </div>
                ))
                : <h2 className='text-center'>Music coming soon!</h2>
              }
            </div>
          </div>
        </Route>
      </Switch>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Music)
