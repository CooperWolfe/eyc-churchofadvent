import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { toDashCase } from '../../../utilities/string.utility'
import './music-card.css'

const mapStateToProps = (state, props) => ({
  music: props.music
})
const mapDispatchToProps = dispatch => ({
})

const propTypes = {
  music: PropTypes.object,
  onDelete: PropTypes.func
}

class MusicCard extends Component {
  handleDelete = e => {
    const { onDelete } = this.props
    e.stopPropagation()
    onDelete(e)
  }

  render() {
    const { music, onDelete } = this.props

    if (!music) return null
    const url = `/media/music/${toDashCase(music.title)}?id=${music.id}`

    return (
      <div className='music-card card bg-light c-primary'>
        <div className='card-body c-primary'>
          <Link className='c-primary' to={url}
                onClick={e => e.stopPropagation()}>
            <h5 className='card-title c-primary d-inline-block'>"{ music.title }"</h5>
          </Link>
          <h6 className='card-subtitle mb-2 c-accent'>{ music.date.toLocaleDateString() }</h6>
          <p className='card-text'>{ music.description }</p>
        </div>
        { music.files && (
          <ul className='list-group list-group-flush'>
            { music.files.map((file, i) => (
              <li key={i} className='list-group-item bg-accent'>
                <a onClick={e => e.stopPropagation()} className='c-light' href={file.url}>
                  { file.description } <i className='fa fa-external-link'/>
                </a>
              </li>
            )) }
          </ul>
        ) }
        { !!onDelete && (
          <div className='card-body'>
            <button className='btn btn-outline-danger w-100'
                    onClick={e => e.stopPropagation()}
                    onDoubleClick={this.handleDelete}><i className='fa fa-trash'/> (double-click)</button>
          </div>
        ) }
      </div>
    )
  }
}

MusicCard.propTypes = propTypes

export default connect(mapStateToProps, mapDispatchToProps)(MusicCard)
