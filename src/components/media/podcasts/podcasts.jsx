import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modules } from '../../../stores/app.modules'
import { fetchPodcasts } from '../../../stores/podcasts/podcast.actions'
import PodcastCard from './podcast-card'
import { Route, Switch } from 'react-router-dom'

const mapStateToProps = state => ({
  podcasts: state[Modules.PODCASTS].list
})
const mapDispatchToProps = dispatch => ({
  fetchPodcasts: () => dispatch(fetchPodcasts())
})

class Podcasts extends Component {
  state = {}

  componentDidMount() {
    const { fetchPodcasts } = this.props
    fetchPodcasts()
  }

  render() {
    const { podcasts, match: { path }, location: { search } } = this.props

    const match = search.match(/\?id=([a-f0-9-]+)/)
    const id = match && match[1]
    const podcast = podcasts.filter(p => p.id === id)[0]

    return (
      <Switch>
        <Route path={`${path}/:title`}>{ podcast && (
          <div className='podcast text-center' itemScope itemType='http://schema.org/RadioSeries'>
            { podcast && <PodcastCard podcast={podcast} /> }
          </div>
        ) }</Route>
        <Route path={path}>
          <div className='podcasts container'>
            <h1 className='text-center pt-3'>Podcasts</h1>
            <div className='row' itemScope itemType='http://schema.org/RadioSeries'>
              { podcasts.map((podcast, i) => (
                <div key={i} className='col-12 col-sm-6 col-lg-4 mt-3'>
                  <PodcastCard podcast={podcast}/>
                </div>
              )) }
            </div>
          </div>
        </Route>
      </Switch>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Podcasts)
