import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { toDashCase } from '../../../utilities/string.utility'
import './podcast-card.css'

const mapStateToProps = (state, props) => ({
  podcast: props.podcast
})
const mapDispatchToProps = dispatch => ({
})

const propTypes = {
  podcast: PropTypes.object,
  onDelete: PropTypes.func
}

class PodcastCard extends Component {
  handleDelete = e => {
    const { onDelete } = this.props
    e.stopPropagation()
    onDelete(e)
  }

  render() {
    const { podcast, onDelete } = this.props

    if (!podcast) return null
    const url = `/media/podcasts/${toDashCase(podcast.title)}?id=${podcast.id}`

    return (
      <div className='podcast-card card bg-light c-primary'
           itemScope itemProp='episode' itemType='http://schema.org/RadioEpisode'>
        <div className='card-body c-primary'>
          <Link className='c-primary' to={url}
                onClick={e => e.stopPropagation()}
                itemProp='url'>
            <h5 className='card-title c-primary d-inline-block'
                itemProp='name'>"{ podcast.title }"</h5>
          </Link>
          <h6 className='card-subtitle mb-2 c-accent'
              itemProp='datePublished'>{ podcast.date.toLocaleDateString() }</h6>
          <p className='card-text'
             itemProp='description'>{ podcast.description }</p>
        </div>
        { podcast.files && (
          <ul className='list-group list-group-flush'>
            { podcast.files.map((file, i) => (
              <li key={i} className='list-group-item bg-accent'
                  itemScope itemProp='publication' itemType='http://schema.org/PublicationEvent'>
                <a onClick={e => e.stopPropagation()} className='c-light' href={file.url}
                   itemProp='url'>{ file.description } <i className='fa fa-external-link'/></a>
              </li>
            )) }
          </ul>
        ) }
        { !!onDelete && (
          <div className='card-body'>
            <button className='btn btn-outline-danger w-100'
                    onClick={e => e.stopPropagation()}
                    onDoubleClick={this.handleDelete}><i className='fa fa-trash'/> (double-click)</button>
          </div>
        ) }
      </div>
    )
  }
}

PodcastCard.propTypes = propTypes

export default connect(mapStateToProps, mapDispatchToProps)(PodcastCard)
