import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchEvents } from '../../stores/event/event.actions'
import { toSmallTimeString } from '../../utilities/date.utility'
import history from '../../services/history.service'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  fetchEvents: () => dispatch(fetchEvents())
})

class EventSummary extends Component {
  state = {
    event: {}
  }

  componentDidMount() {
    const { fetchEvents, location } = this.props
    const id = location.search.match(/id=([a-z0-9-]*)/)[1]
    fetchEvents().then(res => {
      const event = res.list.filter(e => e.id === id)[0]
      if (event) {
        this.setState({ event })
      }
      else {
        history.push('/not-found')
      }
    })
  }

  renderDates = () => {
    const { event } = this.state

    if (!event.start || !event.end) {
      return null
    }

    const text = event.start.toLocaleDateString() === event.end.toLocaleDateString()
      ? `on ${event.start.toLocaleDateString()}`
      : `on ${event.start.toLocaleDateString()} until ${toSmallTimeString(event.end)}`

    return (
      <div className='dates'>
        <em className='c-light h5'>{ text }</em>
      </div>
    )
  }
  renderTime = () => {
    const { event } = this.state

    if (!event.start || !event.end) {
      return null
    }

    const on = `${event.start.toLocaleDateString()} at ${ toSmallTimeString(event.start) }`
    const until = event.start.toLocaleDateString() === event.end.toLocaleDateString()
      ? toSmallTimeString(event.end)
      : `${event.end.toLocaleDateString()} at ${toSmallTimeString(event.end)}`

    return (
      <div className='time'>
        <em className='c-light h5'>on { on } until { until }</em>
      </div>
    )
  }
  render() {
    const { event } = this.state
    return (
      <div className='event container'>
        <div className='text-center c-light'>
          <h3 className='p-2 c-accent'>Come join us for ...</h3>
          <h1>{ event.title }</h1>
          { event.allDay ? this.renderDates() : this.renderTime() }
          <hr/>
          <p>{ event.description }</p>
          <hr/>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EventSummary)
