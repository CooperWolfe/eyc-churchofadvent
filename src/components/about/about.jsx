import React, { Component } from 'react'
import { connect } from 'react-redux'
import CoreValue from './core-value'
import YouthMinister from './youth-minister'
import { Modules } from '../../stores/app.modules'
import { fetchYouthMinisters } from '../../stores/youth-ministers/youthMinisters.actions'
import Loader from '../shared/loader'

const mapStateToProps = state => ({
  loading: state[Modules.YOUTH_MINISTERS].loading,
  youthMinisters: state[Modules.YOUTH_MINISTERS].list || []
})
const mapDispatchToProps = dispatch => ({
  fetchYouthMinisters: () => dispatch(fetchYouthMinisters())
})

class About extends Component {
  state = {}

  componentDidMount() {
    const { fetchYouthMinisters } = this.props
    fetchYouthMinisters()
  }

  render() {
    const { youthMinisters, loading } = this.props

    return (
      <div className='about'>
        <h1 className='text-center mt-3 mb-3'>
          <img src='/logo-simple-purple.png' style={{ maxWidth: 40 }} /> What we're all about
        </h1>
        <CoreValue className='growth' imageUrl='growth.png' backgroundUrl='https://firebasestorage.googleapis.com/v0/b/eyc-churchofadvent.appspot.com/o/growth.jpeg?alt=media&token=19b4569c-a6e1-4546-bfb5-15fc07e38201' offset='-40%'>
          We believe in creating and sustaining a culture that welcomes youth where they are while challenging them
          to grow more into who they were born to be by taking their next step in their personal relationship with
          Jesus Christ.
        </CoreValue>
        <CoreValue className='community' imageUrl='community.png' backgroundUrl='https://firebasestorage.googleapis.com/v0/b/eyc-churchofadvent.appspot.com/o/community.jpeg?alt=media&token=44cbcaeb-804b-414a-adc0-34a831231cdc'>
          We believe that God created humans to have relationships with one another to feel supported, valued, and
          loved. We believe in creating an environment not of fear but of love that welcomes all students from all
          backgrounds and gives them a safe and friendly place to socialize and connect with others.
        </CoreValue>
        <CoreValue className='love' imageUrl='love.png' backgroundUrl='https://firebasestorage.googleapis.com/v0/b/eyc-churchofadvent.appspot.com/o/love.jpeg?alt=media&token=eedb7448-f6a2-41af-a708-4e6df515cf73'>
          We believe that there is no fear in love, but perfect love casts out fear. For fear has to do with
          punishment, and whoever fears has not been perfected in love. Through EYC we provide many new opportunities
          for our students to question their own ideas and experiences of love with an attitude openness towards truth.
        </CoreValue>
        <CoreValue className='discipleship' imageUrl='discipleship.png' backgroundUrl='https://firebasestorage.googleapis.com/v0/b/eyc-churchofadvent.appspot.com/o/discipleship.png?alt=media&token=8d18a5b0-3bbc-4e86-9442-f66fee6b8c7c'>
          We believe the journey of discipleship is going deeper and deeper into revelation through faith and
          experience. We believe revelation is to see something in the detail of what it truly is and by providing
          opportunities for students to explore what has been revealed to them will give them experience that will
          strengthen their faith.
        </CoreValue>
        <h1 className='text-center mt-2'>Meet Our Youth Ministers!</h1>
        <hr style={{ width: 500 }}/>
        { loading !== 0 && <div className='text-center pb-3'><Loader className='c-light' style={{ fontSize: '3rem' }} /></div> }
        { loading === 0 && youthMinisters.map((youthMinister, i) => (
          <YouthMinister key={i}
                         name={`${youthMinister.firstName} ${youthMinister.lastName}`}
                         imageUrl={youthMinister.imageUrl}>
            { youthMinister.bio }
          </YouthMinister>
        )) }
        <div className='text-center'>
          <div className='embed-responsive embed-responsive-4by3 d-inline-block' style={{ maxWidth: 400 }}>
            <iframe title='Meet the youth ministers'
                    src="https://www.youtube.com/embed/SXbf-lItZAE"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    className='embed-responsive-item'
                    allowFullScreen/>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(About)
