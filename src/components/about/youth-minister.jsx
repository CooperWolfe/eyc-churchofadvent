import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class YouthMinister extends Component {
  state = {}

  render() {
    const { name, imageUrl, children } = this.props

    return (
      <div className='youth minister'>
        <div className='container'>
          <div className='row pt-4 pb-4'>
            <div className='col-3 text-center'>
              <img className='rounded-circle mw-100' src={imageUrl} alt={name}/>
            </div>
            <div className='col-9' style={{ textShadow: 'black 0 0 8px' }}>
              <h2>{ name }</h2>
              <p className='c-light'>{ children }</p>
            </div>
          </div>
        </div>
      </div>
    )
  }

  static propTypes = {
    name: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(YouthMinister)
