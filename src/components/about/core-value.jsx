import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class CoreValue extends Component {
  state = {}

  render() {
    const { className, imageUrl, children } = this.props

    const borderStyle = '4px solid var(--accent)'

    return (
      <div className={`core value ${className}`} style={{ borderTop: borderStyle, borderBottom: borderStyle }}>
        <div className='bg-light'>
          <div className='container'>
            <div className='row align-items-center pb-5 pt-5'>
              <div className='col' style={{ maxWidth: 150 }}>
                <img className='d-block w-100' src={imageUrl} alt={className}/>
              </div>
              <div className='col'>
                <h2 style={{ color: 'black' }}>{ className.toUpperCase() }</h2>
                <p>{ children }</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  static propTypes = {
    className: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    backgroundUrl: PropTypes.string.isRequired
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoreValue)
