import React from 'react'
import { connect } from 'react-redux'
import Snippet from './snippet'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

const isAppleMobile = /iphone|ipod|ipad/.test(navigator.userAgent.toLowerCase())
const mapLink = isAppleMobile
  ? 'http://maps.apple.com/?address=141 Advent Street, Spartanburg, SC 29302'
  : 'https://www.google.com/maps/place/Episcopal+Church+of+the+Advent/@34.9498273,-81.9263847,17z/data=!3m1!4b1!4m5!3m4!1s0x885775891ad5475b:0xd3bee4976bc63d47!8m2!3d34.9498273!4d-81.924196'

class WhoSnippet extends Snippet {
  state = {}
  headerText = 'Who We Are'

  renderContent() {
    const { imageSrc } = this.props

    return (
      <div className='who-we-are'>
        <div className='row'>
          <div className='col' style={{ maxWidth: 130 }}>
            <img src={imageSrc} style={{ maxWidth: '100%' }} />
          </div>
          <div className='col'>
            <h4>The Episcopal Youth Community of The Episcopal Church of the Advent in Spartanburg, SC</h4>
          </div>
        </div>
        <hr/>
        <h3 className='fw-100'>Our church is located at</h3>
        <a className='ff-text c-accent font-weight-bold address' href={mapLink} target='_blank' rel='noopener noreferrer'>
          141 Advent Street, Spartanburg, SC 29302
        </a>
        <a className='btn btn-success mt-3 d-block ml-auto mr-auto' style={{ width: 'fit-content' }} href={mapLink} target='_blank' rel='noopener noreferrer'>
          <div>
            <i className='fa fa-map-marker d-inline-block align-middle pr-1' style={{ fontSize: '2rem' }}/>
            <p className='mb-0 d-inline-block align-middle'>Get Directions</p>
          </div>
        </a>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WhoSnippet)
