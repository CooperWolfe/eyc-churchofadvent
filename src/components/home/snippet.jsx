import React, { Component } from 'react'
import './snippet.css'

class Snippet extends Component {
  state = {}

  headerText = ''

  render() {
    return (
      <div className='snippet'>
        <div className='snippet-header'>
          <h2>{ this.headerText }</h2>
          <hr/>
        </div>
        <div className='snippet-content'>
          { this.renderContent && this.renderContent() }
        </div>
      </div>
    )
  }
}

export default Snippet
