import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchPosts } from '../../stores/instagram/instagram.actions'
import { Modules } from '../../stores/app.modules'

const mapStateToProps = state => ({
  posts: state[Modules.INSTAGRAM].posts
})
const mapDispatchToProps = dispatch => ({
  fetchInstagramPosts: () => dispatch(fetchPosts())
})

class Instagram extends Component {

  componentDidMount() {
    this.props.fetchInstagramPosts()
  }

  renderPost = (post, i) => {

    const imageUrl = post.images.standard_resolution.url;
    const caption = (post.caption && post.caption.text) || ''
    const link = post.link
    const createdTime = new Date(+post.created_time * 1000)

    return (
      <a className='post col-lg-6 text-left c-light' key={i} href={link} target='_blank' rel='noopener noreferrer'>
        <div className='container'>
          <div className='row'>
            <div className='col-sm-3 col-5'>
              <p>{ createdTime.toLocaleDateString() }</p>
              <img src={imageUrl} alt={`Instagram post from ${createdTime.toDateString()}`} style={{ width: 100, borderRadius: '.5rem' }} />
            </div>
            <div className='col-sm-9 col-7 overflow-hidden'>
              <p>{ caption }</p>
            </div>
          </div>
          <hr style={{ border: '1px solid var(--accent)' }}/>
        </div>
      </a>
    )
  }

  render() {
    const { posts } = this.props

    return (
      <div className='instagram text-center pt-5'>
        <h1>Stay Connected! <a href='https://www.instagram.com/adventeyc' target='_blank' rel='noopener noreferrer'>
          <i className='fa fa-instagram c-light'/>
        </a></h1>
        <div className='container mt-5 mb-3'>
          <div className='row'>
            { posts && posts.map(this.renderPost) }
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Instagram)
