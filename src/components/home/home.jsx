import React, { Component } from 'react'
import { connect } from 'react-redux'
import WhoSnippet from './whoSnippet'
import ContactSnippet from './contactSnippet'
import Instagram from './instagram'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class Home extends Component {
  state = {}

  render() {
    return (
      <div className='home'>
        <div className='container pt-md-4 pb-md-4'>
          <div className='row'>
            <div className='col-md-6 text-center p-4'><WhoSnippet imageSrc='/logo.png'/></div>
            <div className='col-md-6 text-center p-4'><ContactSnippet/></div>
          </div>
        </div>
        <Instagram/>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
