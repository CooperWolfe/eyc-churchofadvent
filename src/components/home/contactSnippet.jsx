import React from 'react'
import { connect } from 'react-redux'
import Snippet from './snippet'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class ContactSnippet extends Snippet {
  state = {}
  headerText = 'Connect With Us'

  renderLi = (icon, link, text) => {
    return (
      <li className='list-group-item bg-primary'>
        <a href={link} target='_blank' rel='noopener noreferrer' className='d-flex'>
          <i className={`fa fa-${icon} float-left c-light`} style={{ fontSize: '3rem' }}/>
          <span style={{ lineHeight: '3rem' }} className='c-light font-weight-bold m-auto'>{ text }</span>
        </a>
      </li>
    )
  }

  renderContent = () => {
    return (
      <div className='contact-snippet'>
        <ul className='list-group list-group-flush'>
          { this.renderLi('facebook-square', 'https://www.facebook.com/adventeycc/', 'Facebook') }
          { this.renderLi('instagram', 'https://www.instagram.com/adventeyc/', 'Instagram') }
          <li className='list-group-item bg-transparent'>
            <a href='mailto:eyc@churchofadvent.org' className='d-flex'>
              <i className={`fa fa-envelope float-left c-light`} style={{ fontSize: '3rem' }}/>
              <span style={{ wordBreak: 'break-word' }} className='c-light font-weight-bold m-auto'>eyc@churchofadvent.org</span>
            </a>
          </li>
        </ul>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactSnippet)
