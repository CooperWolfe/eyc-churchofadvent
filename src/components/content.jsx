import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import NotFound from './notFound'
import Home from './home/home'
import About from './about/about'
import Connect from './connect/connect'
import Login from './login/login'
import Logout from './logout/logout'
import Admin from './admin/admin'
import Events from './events/events'
import EventSummary from './event/event-summary'
import Media from './media/media'
import Forms from './parent-portal/forms/forms'
import Serve from './parent-portal/serve/serve'

class Content extends Component {
  render() {
    return (
      <Switch>
        <Route path='/' component={Home} exact={true} />

        <Route path='/event/:title' component={EventSummary} />

        <Route path='/about' component={About} />
        <Route path='/connect' component={Connect} />
        <Route path='/login' component={Login} />
        <Route path='/logout' component={Logout} />
        <Route path='/admin' component={Admin} />
        <Route path='/events' component={Events}/>
        <Route path='/media' component={Media}/>
        <Route path='/forms' component={Forms}/>
        <Route path='/serve' component={Serve}/>

        <Route path='/not-found' render={() => <NotFound className='c-light' linkStyle={{ color: 'var(--accent)' }}/>}/>
        <Redirect from='/' to='/not-found' />
      </Switch>
    )
  }
}

export default Content
