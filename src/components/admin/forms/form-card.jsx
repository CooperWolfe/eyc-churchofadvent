import React from 'react'
import { connect } from 'react-redux'
import './form-card.css'
import FileForm from '../../../helpers/fileForm'
import { deleteFile, uploadFile } from '../../../stores/file/file.actions'
import { deleteForm, postForm, storeForm } from '../../../stores/forms/forms.actions'
import FormFactory from '../../../model/factories/form.factory'
import { Modules } from '../../../stores/app.modules'
import logService from '../../../services/log.service'
import Loader from '../../shared/loader'
import Form from '../../../model/form.model'

const mapStateToProps = state => ({
  loading: state[Modules.FORMS].loading > 0,
  fileLoading: state[Modules.FILE].loading > 0
})
const mapDispatchToProps = dispatch => ({
  deleteFile: url => dispatch(deleteFile(url)),
  deleteForm: id => dispatch(deleteForm(id)),
  uploadFile: file => dispatch(uploadFile(file)),
  addForm: form => dispatch(storeForm(form)),
  updateForm: form => dispatch(postForm(form))
})

class FormCard extends FileForm {
  state = {
    ...FileForm.defaultState,
    data: FormFactory.create(),
    isNew: true
  }

  static getDerivedStateFromProps(props, state) {
    if (!props.form || props.form._id === state.data._id) return null
    return {
      data: props.form.clone(),
      isNew: false
    }
  }

  handleDelete = () => {
    const { form, deleteFile, deleteForm } = this.props
    deleteFile(form.url)
    deleteForm(form.id)
  }
  doChange = e => {
    if (e.currentTarget.name === 'url') {
      this.doSubmit()
    }
  }
  doSubmit = () => {
    const { addForm, updateForm } = this.props
    let { isNew, data } = this.state

    data = Form.fromObject(data)

    if (isNew) addForm(data).then(() => {
      this.setState({ data: FormFactory.create() })
    })
    else {
      updateForm(data).then(() => {
        logService.alert('Saved form successfully!')
      })
    }
  }

  render() {
    const { isNew, data } = this.state
    const { fileLoading, loading } = this.props

    if (!data) return <h1>Hi</h1>

    return (
      <div className='form-card card bg-light c-primary'>
        <div className='card-body c-primary'>
          { this.renderLabellessField('Form Title', 'title', 'text') }
          { isNew && this.renderFileUpload(
            fileLoading ? <Loader /> : 'Upload & Save',
            'url',
            'new-file',
            'success',
            '*',
            'w-100 mb-0'
          ) }
          { !isNew && (
            <div className='existing-form'>
              <ul className='list-group list-group-flush'>
                <li className='list-group-item text-center bg-transparent pt-0'>
                  <a href={data.url}><i className='fa fa-external-link'/></a>
                </li>
              </ul>
              <div className='btn-group w-100'>
                <button className='btn btn-danger w-50' onClick={this.handleDelete}>Delete</button>
                <button className='btn btn-success w-50' onClick={this.doSubmit}>{ loading ? <Loader /> : 'Save' }</button>
              </div>
            </div>
          ) }
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormCard)
