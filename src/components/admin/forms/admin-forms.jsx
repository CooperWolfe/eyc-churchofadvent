import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { fetchForms } from '../../../stores/forms/forms.actions'
import { Modules } from '../../../stores/app.modules'
import FormCard from './form-card'

const mapStateToProps = state => ({
  forms: state[Modules.FORMS].list
})
const mapDispatchToProps = dispatch => ({
  fetchForms: () => dispatch(fetchForms())
})

class AdminForms extends Component {
  state = {}

  componentDidMount() {
    const { fetchForms } = this.props
    fetchForms()
  }

  render() {
    const { forms } = this.props

    return (
      <div className='admin-forms container'>
        <div className='text-center mt-2'>
          <h1>Manage Forms</h1>
          <NavLink className='c-light' to='/forms'>Forms <i className='fa fa-external-link'/></NavLink>
        </div>
        <hr/>
        <div className='form-list'>
          <div className='row'>
            { forms.map((form, i) => (
              <div key={i} className='col-12 col-sm-6 col-md-4 col-lg-3 mt-3 text-left p-2 cursor-pointer'>
                <FormCard form={form} />
              </div>
            )) }
            <div className='col-12 col-sm-6 col-md-4 col-lg-3 mt-3 text-left p-2 cursor-pointer'>
              <FormCard />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminForms)
