import React from 'react'
import { connect } from 'react-redux'
import Form from '../../../../helpers/form'
import Joi from 'joi-browser'
import history from '../../../../services/history.service'
import MusicFactory from '../../../../model/factories/music.factory'
import { storeMusic } from '../../../../stores/music/music.actions'
import { Modules } from '../../../../stores/app.modules'
import Loader from '../../../shared/loader'
import Music from '../../../../model/music.model'

const mapStateToProps = state => ({
  loading: state[Modules.MUSIC].loading > 0
})
const mapDispatchToProps = dispatch => ({
  addMusic: music => dispatch(storeMusic(music))
})

class AddMusicForm extends Form {
  state = {
    ...Form.defaultState,
    data: MusicFactory.create()
  }
  schema = {
    title: Joi.string().required().error(() => ({ message: 'Title cannot be empty' })),
    date: Joi.date().required().error(() => ({ message: 'Date cannot be empty' })),
    description: Joi.string().allow('')
  }

  render() {
    const { loading } = this.props

    return (
      <div className='add music form container text-center'>
        <div className='row'>
          <div className='col- col-md-3'/>
          <div className='col-12 col-md-6'>
            <h2 className='c-accent'>Add Music</h2>
            <form onSubmit={this.handleSubmit} className='text-left'>
              { this.renderField('Title', 'title', 'text') }
              { this.renderDateTimePicker('Date', 'date', '', false) }
              { this.renderTextArea('Description', 'description', 2) }
              <div className='btn-group'>
                { this.renderButton('submit', 'success', loading ? <Loader/> : 'Save') }
                { this.renderButton('button', 'dark', 'Clear', '', this.handleClear) }
              </div>
            </form>
          </div>
          <div className='col- col-md-3'/>
        </div>
      </div>
    )
  }

  doSubmit = () => {
    const { addMusic } = this.props
    const { data } = this.state
    addMusic(Music.fromObject(data)).then(() => {
      history.push('/admin/media/music')
    })
  }

  handleClear = () => {
    this.setState({ data: MusicFactory.create() })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddMusicForm)
