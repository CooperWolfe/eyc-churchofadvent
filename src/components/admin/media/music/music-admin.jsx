import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import history from '../../../../services/history.service'
import { Modules } from '../../../../stores/app.modules'
import { deleteMusic, fetchMusic } from '../../../../stores/music/music.actions'
import MusicCard from '../../../media/music/music-card'
import { deleteFile } from '../../../../stores/file/file.actions'

const mapStateToProps = state => ({
  music: state[Modules.MUSIC].list
})
const mapDispatchToProps = dispatch => ({
  fetchMusic: () => dispatch(fetchMusic()),
  deleteMusic: id => dispatch(deleteMusic(id)),
  deleteFile: url => dispatch(deleteFile(url))
})

class PodcastsAdmin extends Component {
  state = {}

  componentDidMount() {
    const { fetchMusic } = this.props
    fetchMusic()
  }

  handleDelete = id => {
    const { music, deleteMusic, deleteFile } = this.props
    const item = music.filter(m => m.id === id)[0]
    if (!item) return
    item.files && item.files
      .map(file => file.url)
      .filter(url => url.startsWith('https://firebasestorage.googleapis.com/v0/b/eyc-churchofadvent.appspot.com/'))
      .forEach(url => deleteFile(url))
    deleteMusic(id)
  }

  render() {
    const { music } = this.props

    return <div className='container mt-3 text-center'>
      <h1>Manage Music</h1>
      <NavLink className='c-light' to='/media/music'>Music <i className='fa fa-external-link'/></NavLink>
      <hr/>

      <div className='row'>
        {music.map((item, i) => (
          <div key={i} onClick={() => history.push(`/admin/media/music/${item.id}`)}
               className='col-12 col-sm-6 col-md-4 col-lg-3 mt-3 text-left p-2'
               style={{ cursor: 'pointer' }}>
            <MusicCard music={item} onDelete={() => this.handleDelete(item.id)}/>
          </div>
        ))}
        <NavLink to='/admin/media/music/add' className='col-12 col-sm-6 col-md-4 col-lg-3 mt-3 p-2'>
          <div className='card bg-success'>
            <div className='card-body c-light'>
              <h1 className='card-title mb-0'><i className='fa fa-plus'/></h1>
            </div>
          </div>
        </NavLink>
      </div>
    </div>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PodcastsAdmin)
