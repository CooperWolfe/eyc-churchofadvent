import React from 'react'
import { connect } from 'react-redux'
import FileForm from '../../../../helpers/fileForm'
import VideoFactory from '../../../../model/factories/video.factory'
import Joi from 'joi-browser'
import { Modules } from '../../../../stores/app.modules'
import { deleteVideo, fetchVideos, storeVideo } from '../../../../stores/videos/videos.actions'
import logService from '../../../../services/log.service'
import Loader from '../../../shared/loader'
import { deleteFile, uploadFile } from '../../../../stores/file/file.actions'
import Video from '../../../../model/video.model'
import Videos from '../../../media/videos/video-gallery'
import { NavLink } from 'react-router-dom'

const mapStateToProps = state => ({
  loading: state[Modules.VIDEOS].loading > 0,
  fileLoading: state[Modules.FILE].loading > 0,
  videos: state[Modules.VIDEOS].list.sort((lhs, rhs) => lhs.date > rhs.date)
})
const mapDispatchToProps = dispatch => ({
  addVideo: video => dispatch(storeVideo(video)),
  uploadFile: file => dispatch(uploadFile(file)),
  fetchVideos: () => dispatch(fetchVideos()),
  deleteFile: url => dispatch(deleteFile(url)),
  deleteVideo: id => dispatch(deleteVideo(id))
})

class VideosAdmin extends FileForm {
  state = {
    ...FileForm.defaultState,
    data: {
      ...VideoFactory.create(),
      upload: ''
    }
  }
  schema = {
    date: Joi.date().required().error(() => ({ message: 'Date cannot be empty' })),
  }

  handleClear = () => {
    this.setState({
      data: {
        ...VideoFactory.create(),
        upload: ''
      }
    })
  }
  handleDelete = id => {
    const { videos, deleteFile, deleteVideo } = this.props
    const video = videos.filter(v => v.id === id)[0]
    if (!video) return
    if (video.url.startsWith('https://firebasestorage.googleapis.com/v0/b/eyc-churchofadvent.appspot.com/')) {
      deleteFile(video.url)
    }
    deleteVideo(video.id)
  }

  componentDidMount() {
    const { fetchVideos } = this.props
    fetchVideos()
  }
  render() {
    const { data } = this.state
    const { loading, fileLoading, videos } = this.props

    return (
      <div className='videos admin'>
        <div className='header text-center pb-3'>
          <h2>Manage Videos</h2>
          <NavLink to='/media/videos' className='c-light'>Videos <i className='fa fa-external-link'/></NavLink>
        </div>
        <div className='container'><Videos videos={videos} onDelete={this.handleDelete} defaultText='No Videos'/></div>
        <form className='add videos form col-md-6 m-auto text-center'>
          { this.renderDateTimePicker('Date', 'date', '', false) }
          { this.renderField('URL (optional)', 'url', 'text') }
          <div className='btn-group'>
            { !data.url && this.renderFileUpload(fileLoading ? <Loader/> : 'Upload & Save', 'upload', 'file', 'success', 'video/*') }
            { data.url && this.renderButton('button', 'success', loading ? <Loader/> : 'Save', '', this.handleSubmit) }
            { this.renderButton('button', 'dark', 'Clear', 'h-100', this.handleClear) }
          </div>
        </form>
      </div>
    )
  }

  doChange = e => {
    const { data } = this.state

    if (e.currentTarget.name === 'upload') {
      this.setState({
        data: {
          ...data,
          url: data.upload
        }
      }, this.doSubmit)
    }
  }

  doSubmit = () => {
    const { addVideo } = this.props
    const { data } = this.state

    addVideo(Video.fromObject(data)).then(this.handleClear).then(() => {
      logService.alert('Saved video successfully')
    })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideosAdmin)
