import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import PodcastCard from '../../../media/podcasts/podcast-card'
import history from '../../../../services/history.service'
import { deletePodcast, fetchPodcasts } from '../../../../stores/podcasts/podcast.actions'
import { Modules } from '../../../../stores/app.modules'
import { deleteFile } from '../../../../stores/file/file.actions'

const mapStateToProps = state => ({
  podcasts: state[Modules.PODCASTS].list
})
const mapDispatchToProps = dispatch => ({
  fetchPodcasts: () => dispatch(fetchPodcasts()),
  deletePodcast: id => dispatch(deletePodcast(id)),
  deleteFile: url => dispatch(deleteFile(url))
})

class PodcastsAdmin extends Component {
  state = {}

  componentDidMount() {
    const { fetchPodcasts } = this.props
    fetchPodcasts()
  }

  handleDelete = id => {
    const { podcasts, deletePodcast, deleteFile } = this.props
    const podcast = podcasts.filter(m => m.id === id)[0]
    if (!podcast) return
    podcast.files && podcast.files
      .map(file => file.url)
      .filter(url => url.startsWith('https://firebasestorage.googleapis.com/v0/b/eyc-churchofadvent.appspot.com/'))
      .forEach(url => deleteFile(url))
    deletePodcast(id)
  }

  render() {
    const { podcasts } = this.props

    return <div className='container mt-3 text-center'>
      <h1>Manage Podcasts</h1>
      <NavLink className='c-light' to='/media/podcasts'>Podcasts <i className='fa fa-external-link'/></NavLink>
      <hr/>

      <div className='row'>
        {podcasts.map((podcast, i) => (
          <div key={i} onClick={() => history.push(`/admin/media/podcasts/${podcast.id}`)}
               className='col-12 col-sm-6 col-md-4 col-lg-3 mt-3 text-left p-2'
               style={{ cursor: 'pointer' }}>
            <PodcastCard podcast={podcast} onDelete={() => this.handleDelete(podcast.id)}/>
          </div>
        ))}
        <NavLink to='/admin/media/podcasts/add' className='col-12 col-sm-6 col-md-4 col-lg-3 mt-3 p-2'>
          <div className='card bg-success'>
            <div className='card-body c-light'>
              <h1 className='card-title mb-0'><i className='fa fa-plus'/></h1>
            </div>
          </div>
        </NavLink>
      </div>
    </div>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PodcastsAdmin)
