import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import Joi from 'joi-browser'
import logService from '../../../../services/log.service'
import Loader from '../../../shared/loader'
import { Modules } from '../../../../stores/app.modules'
import { fetchPodcasts, postPodcast } from '../../../../stores/podcasts/podcast.actions'
import Podcast from '../../../../model/podcast.model'
import FileForm from '../../../../helpers/fileForm'
import { deleteFile, uploadFile } from '../../../../stores/file/file.actions'

const mapStateToProps = state => ({
  loading: state[Modules.PODCASTS].loading > 0,
  fileLoading: state[Modules.FILE].loading > 0
})
const mapDispatchToProps = dispatch => ({
  fetchPodcasts: () => dispatch(fetchPodcasts()),
  updatePodcast: podcast => dispatch(postPodcast(podcast)),
  uploadFile: file => dispatch(uploadFile(file)),
  deleteFile: url => dispatch(deleteFile(url))
})

class UpdatePodcastForm extends FileForm {
  state = {
    ...FileForm.defaultState,
    data: {
      ...(this.props.podcast || {}),
      newFile: '',
      fileName: '',
      linkUrl: '',
      linkName: ''
    }
  }
  schema = {
    title: Joi.string().required().error(() => ({ message: 'Title cannot be empty' })),
    date: Joi.date().required().error(() => ({ message: 'Date cannot be empty' })),
  }
  
  static getDerivedStateFromProps(props, state) {
    if (!!state.data._id) {
      return null
    }

    return {
      data: {
        ...state.data,
        ...props.podcast
      }
    }
  }

  componentDidMount() {
    const { fetchPodcasts } = this.props
    fetchPodcasts()
  }

  render() {
    const { loading, fileLoading, podcast } = this.props
    if (!podcast) return null

    const files = this.props.podcast.files

    return (
      <Fragment>
        <h2 className='c-accent text-center'>Update Podcast</h2>

        {/*Podcast form*/}
        <form onSubmit={this.handleSubmit} className='col-md-6 m-auto'>
          { this.renderField('Title', 'title', 'text', 'text-center') }
          { this.renderDateTimePicker('Date', 'date', '', false) }
          { this.renderTextArea('Description', 'description', 2) }
          { this.renderButton('submit', 'success', loading ? <Loader /> : 'Save') }
        </form>

        {/*File upload form*/}
        <hr/>
        <h2 className='c-accent text-center'>Add Links</h2>
        <div className='col-md-8 m-auto'>
          <ul className='list-group'>
            { files && files.map((file, i) => (
              <li key={i} className='list-group-item text-left bg-light'>
                <a className='c-accent' key={i} href={file.url}>{ file.description } <i className='fa fa-external-link'/></a>
                <i onClick={() => this.handleDelete(i)} className='fa fa-remove float-right c-primary o-hover-50'/>
              </li>
            )) }
            <li className='list-group-item text-left bg-light pb-0'>
              <div className='m-auto'>
                { this.renderLabellessField('Link name', 'linkName', 'text') }
              </div>
              <div className='m-auto'>
                { this.renderLabellessField('Link URL', 'linkUrl', 'text') }
              </div>
              <div className='text-center mb-3'>
                { this.renderButton('button', 'success', loading ? <Loader/> : 'Add link', '', this.handleAddLink) }
              </div>
            </li>
            <li className='list-group-item text-left bg-light pb-0'>
              <div className='col-8 d-inline-block'>
                { this.renderLabellessField('File name', 'fileName', 'text') }
              </div>
              <div className='col-4 d-inline-block text-center'>
                { this.renderFileUpload(fileLoading ? <Loader /> : 'Choose File', 'newFile', 'newFile', 'success') }
              </div>
            </li>
          </ul>
        </div>
      </Fragment>
    )
  }

  handleAddLink = () => {
    const { data, data: { linkUrl, linkName } } = this.state

    if (this.validate()) return

    this.setState({
      data: {
        ...data,
        files: [
          ...data.files,
          { url: linkUrl, description: linkName }
        ],
        linkUrl: '',
        linkName: ''
      }
    }, this.doSubmit)
  }
  doSubmit = () => {
    const { updatePodcast } = this.props
    const { data } = this.state

    updatePodcast(Podcast.fromObject(data)).then(() => {
      logService.alert('Successfully updated podcast')
    })
  }
  doChange = e => {
    const { data, data: { newFile, fileName } } = this.state

    if (e.currentTarget.name === 'newFile') {
      this.setState({
        data: {
          ...data,
          files: [
            ...data.files,
            { url: newFile, description: fileName }
          ],
          newFile: '',
          fileName: ''
        }
      }, this.doSubmit)
    }
  }
  handleDelete = fileIndex => {
    let { deleteFile } = this.props
    const { data, data: { files } } = this.state

    if (!files[fileIndex].url.startsWith('https://firebasestorage.googleapis.com/v0/b/eyc-churchofadvent.appspot.com/')) {
      deleteFile = _ => Promise.resolve(true)
    }

    deleteFile(files[fileIndex].url).then(() => {
      this.setState({
        data: {
          ...data,
          files: [
            ...data.files.slice(0, fileIndex),
            ...data.files.slice(fileIndex + 1)
          ],
          newFile: '',
          fileName: ''
        }
      }, this.doSubmit)
    })
  }
  handleFileDescriptionChange = e => this.setState({ fileDescription: e.target.value })
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePodcastForm)
