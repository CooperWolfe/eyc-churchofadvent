import React from 'react'
import { connect } from 'react-redux'
import Form from '../../../../helpers/form'
import Joi from 'joi-browser'
import history from '../../../../services/history.service'
import { storePodcast } from '../../../../stores/podcasts/podcast.actions'
import PodcastFactory from '../../../../model/factories/podcast.factory'
import { Modules } from '../../../../stores/app.modules'
import Loader from '../../../shared/loader'
import Podcast from '../../../../model/podcast.model'

const mapStateToProps = state => ({
  loading: state[Modules.PODCASTS].loading > 0
})
const mapDispatchToProps = dispatch => ({
  addPodcast: podcast => dispatch(storePodcast(podcast))
})

class AddPodcastForm extends Form {
  state = {
    ...Form.defaultState,
    data: PodcastFactory.create()
  }
  schema = {
    title: Joi.string().required().error(() => ({ message: 'Title cannot be empty' })),
    date: Joi.date().required().error(() => ({ message: 'Date cannot be empty' })),
    description: Joi.string().allow('')
  }

  render() {
    const { loading } = this.props

    return (
      <div className='add podcast form container text-center'>
        <div className='row'>
          <div className='col- col-md-3'/>
          <div className='col-12 col-md-6'>
            <h2 className='c-accent'>Add Podcast</h2>
            <form onSubmit={this.handleSubmit} className='text-left'>
              { this.renderField('Title', 'title', 'text') }
              { this.renderDateTimePicker('Date', 'date', '', false) }
              { this.renderTextArea('Description', 'description', 2) }
              <div className='btn-group'>
                { this.renderButton('submit', 'success', loading ? <Loader/> : 'Save') }
                { this.renderButton('button', 'dark', 'Clear', '', this.handleClear) }
              </div>
            </form>
          </div>
          <div className='col- col-md-3'/>
        </div>
      </div>
    )
  }

  doSubmit = () => {
    const { addPodcast } = this.props
    const { data } = this.state
    addPodcast(Podcast.fromObject(data)).then(() => {
      history.push('/admin/media/podcasts')
    })
  }

  handleClear = () => {
    this.setState({ data: PodcastFactory.create() })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPodcastForm)
