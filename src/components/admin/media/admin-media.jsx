import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink, Redirect, Route, Switch } from 'react-router-dom'
import './admin-media.css'
import PodcastsAdmin from './podcasts/podcasts-admin'
import MusicAdmin from './music/music-admin'
import VideosAdmin from './videos/videos-admin'
import AddPodcastForm from './podcasts/add-podcast-form'
import AddMusicForm from './music/add-music-form'
import UpdatePodcastForm from './podcasts/update-podcast-form'
import UpdateMusicForm from './music/update-music-form'
import { fetchPodcasts } from '../../../stores/podcasts/podcast.actions'
import { Modules } from '../../../stores/app.modules'
import { fetchMusic } from '../../../stores/music/music.actions'

const mapStateToProps = state => ({
  podcasts: state[Modules.PODCASTS].list,
  music: state[Modules.MUSIC].list
})
const mapDispatchToProps = dispatch => ({
  fetchPodcasts: () => dispatch(fetchPodcasts()),
  fetchMusic: () => dispatch(fetchMusic())
})

class AdminMedia extends Component {
  state = {}

  componentDidMount() {
    const { fetchPodcasts, fetchMusic } = this.props
    fetchPodcasts()
    fetchMusic()
  }

  renderLink = (to, title) => {
    return (
      <li className='list-group-item w-100 bg-primary border-0'>
        <NavLink className='nav-link c-light' to={to}>{ title }</NavLink>
      </li>
    )
  }
  renderSeparator = () => {
    return (
      <li className='mt-3 mb-3' style={{ border: '1px solid var(--light)' }}/>
    )
  }
  render() {
    const { match: { path }, location: { pathname }, podcasts, music } = this.props

    const match = pathname.match(/[^/]+$/)
    const id = match && match[0]
    const podcast = podcasts.filter(p => p.id === id)[0]
    const item = music.filter(m => m.id === id)[0]

    return (
      <div className='media-admin pb-3'>
        <div className='media-admin-header text-center'>
          <ul className='list-group list-group-flush list-group-horizontal w-100'>
            { this.renderLink(`${path}/podcasts`, 'Podcasts') }
            { this.renderSeparator() }
            { this.renderLink(`${path}/music`, 'Music') }
            { this.renderSeparator() }
            { this.renderLink(`${path}/videos`, 'Videos') }
          </ul>
        </div>
        <div className='media-admin-content'>
          <Switch>
            <Route path={`${path}/podcasts/add`} component={AddPodcastForm}/>
            <Route path={`${path}/podcasts/:id`}><UpdatePodcastForm podcast={podcast}/></Route>
            <Route path={`${path}/podcasts`} component={PodcastsAdmin}/>
            <Route path={`${path}/music/add`} component={AddMusicForm}/>
            <Route path={`${path}/music/:id`}><UpdateMusicForm music={item}/></Route>
            <Route path={`${path}/music`} component={MusicAdmin}/>
            <Route path={`${path}/videos`} component={VideosAdmin}/>

            <Redirect from={path} to={`${path}/podcasts`} exact />

            <Redirect to='/not-found' />
          </Switch>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminMedia)
