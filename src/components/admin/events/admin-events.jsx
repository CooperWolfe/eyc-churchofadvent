import React from 'react'
import { connect } from 'react-redux'
import { Modules } from '../../../stores/app.modules'
import { deleteEvent, fetchEvents, postEvent, storeEvent } from '../../../stores/event/event.actions'
import Form from '../../../helpers/form'
import './admin-events.css'
import { Calendar } from 'react-big-calendar'
import localizer from '../../../services/calendarLocalizer.service'
import EventFactory from '../../../model/factories/event.factory'
import logService from '../../../services/log.service'
import Event from '../../../model/event.model'
import Loader from '../../shared/loader'
import { NavLink } from 'react-router-dom'
import Legend from '../../events/legend'
import { today } from '../../../utilities/date.utility'

const mapStateToProps = state => ({
  loading: state[Modules.EVENTS].loading !== 0,
  events: state[Modules.EVENTS].list
})
const mapDispatchToProps = dispatch => ({
  fetchEvents: () => dispatch(fetchEvents()),
  addEvent: event => dispatch(storeEvent(event)),
  updateEvent: event => dispatch(postEvent(event)),
  deleteEvent: id => dispatch(deleteEvent(id))
})

const eventTypeOptions = {
  valueKey: 'id',
  labelKey: 'name',
  options: [
    { id: 'middle-school', name: 'Middle School' },
    { id: 'high-school', name: 'High School' },
    { id: 'college', name: 'College' },
    { id: 'all', name: 'All Students' }
  ]
}

class AdminEvents extends Form {
  state = {
    ...Form.defaultState,
    data: EventFactory.create(),
    isNew: true,
    date: new Date()
  }

  componentDidMount() {
    const { fetchEvents } = this.props
    fetchEvents()
  }
  componentWillUpdate(nextProps, nextState) {
    if (this.state.data.start !== nextState.data.start && !isNaN(nextState.data.start.getTime())) {
      nextState.date = nextState.data.start
    }
  }

  renderButtons = () => {
    const { loading } = this.props
    const { isNew } = this.state

    if (loading) return <Loader className='c-primary ml-3' style={{ fontSize: '1.5rem' }} />

    return (
      <div className='btn-group'>
        { this.renderButton('submit', 'success', isNew ? 'Save' : 'Update') }
        { this.renderButton('button', 'dark', 'Clear', '', this.handleClear) }
        { !isNew && this.renderButton('button', 'danger', 'Delete', '', this.handleDelete) }
      </div>
    )
  }
  renderLink = (to, title) => {
    return <NavLink className='c-light' to={to}>{ title } <i className='fa fa-external-link'/></NavLink>
  }
  render() {
    const { events } = this.props
    const { isNew, data, date } = this.state

    return (
      <div className='admin events'>
        <div className='text-center pt-3'>
          <h2>Manage Events</h2>
          <div className='row'>
            <div className='col-4'>{ this.renderLink('/events/middle-school', 'Middle') }</div>
            <div className='col-4'>{ this.renderLink('/events/high-school', 'High') }</div>
            <div className='col-4'>{ this.renderLink('/events/college', 'College') }</div>
          </div>
        </div>
        <div className='m-3 p-2 pb-5 h-auto rounded bg-light'>
          <div className='row'>
            <div className='p-4 col-lg-4'>
              <form onSubmit={this.handleSubmit}>
                { this.renderHeader(isNew ? 'Create Event' : 'Update Event', 'c-primary') }
                { this.renderField('Title', 'title', 'text', '', 'c-primary') }
                { this.renderDateTimePicker('Start Date', 'start', 'c-primary', !data.allDay) }
                { this.renderDateTimePicker('End Date', 'end', 'c-primary', !data.allDay) }
                { this.renderCheckBox('All Day', 'allDay', '', 'c-primary') }
                { this.renderSelect('Type', 'type', eventTypeOptions, '', 'c-primary') }
                { this.renderTextArea('Description', 'description', 3, '', 'c-primary') }
                { this.renderButtons() }
              </form>
            </div>
            <div className='p-4 col-lg-8'>
              <Calendar localizer={localizer}
                        events={events}
                        date={date}
                        onNavigate={date => this.setState({ date })}
                        selectable={true}
                        eventPropGetter={event => ({ className: Legend.ClassesForType[event.type] })}
                        onSelectSlot={this.handleSelectDates}
                        onSelectEvent={this.handleSelectEvent}/>
              <Legend />
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleClear = () => {
    const data = EventFactory.create()
    data.type = this.state.data.type
    this.setState({ data, isNew: true })
  }
  handleSelectDates = e => {
    let { data } = this.state

    const start = e.slots[0]
    const end = e.slots[e.slots.length - 1]
    const allDay = (end - start) % 86400000 === 0
    if (allDay) end.setHours(12)

    data = Event.fromObject(data)
    this.setState({ data: data.update({ start, end, allDay }) })
  }
  handleSelectEvent = event => {
    this.setState({ data: Event.fromObject(event), isNew: false })
  }
  handleDelete = () => {
    const { data } = this.state
    const { deleteEvent } = this.props
    deleteEvent(data._id).then(() => logService.alert('Delete successful'))
    this.handleClear()
  }

  doChange = e => {
    const { data, data: { end, start } } = this.state

    // Start Date
    if (e.currentTarget.name === 'start' && e.currentTarget.value > end) {
      this.setState({ data: { ...data, end: e.currentTarget.value } })
    }

    // End Date
    else if (e.currentTarget.name === 'end' && e.currentTarget.value < start) {
      this.setState({ data: { ...data, start: e.currentTarget.value } })
    }

    // All Day
    else if (e.currentTarget.name === 'allDay' && end.getHours() === 0) {
      end.setHours(12)
      this.setState({ data: { ...data, end } })
    }
  }

  validateForm = () => {
    const { start, end } = this.state.data
    if (new Date(start).toString() === 'Invalid Date') return 'Invalid Start Date'
    if (new Date(end).toString() === 'Invalid Date') return 'Invalid End Date'
    return null
  }
  doSubmit = () => {
    let { data, isNew } = this.state
    const { addEvent, updateEvent } = this.props

    const error = this.validateForm()
    if (!!error) {
      return logService.alertError(error)
    }

    data = Event.fromObject(data)

    if (isNew) {
      addEvent(data).then(() => {
        this.handleClear()
        logService.alert('Saved successfully')
      })
    }
    else {
      updateEvent(data).then(() => {
        logService.alert('Saved successfully')
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminEvents)
