import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, Route, Switch } from 'react-router-dom'
import YouthMinisters from './youth-ministers/youthMinisters'
import AdminEvents from './events/admin-events'
import AdminForms from './forms/admin-forms'
import AdminMedia from './media/admin-media'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class Admin extends Component {
  state = {}

  render() {
    const { path } = this.props.match

    return (
      <Switch>
        <Route path={`${path}/youth-ministers`} component={YouthMinisters} />
        <Route path={`${path}/events`} component={AdminEvents} />
        <Route path={`${path}/media`} component={AdminMedia} />
        <Route path={`${path}/forms`} component={AdminForms} />

        <Redirect to='/not-found' />
      </Switch>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin)
