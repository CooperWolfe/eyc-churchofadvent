import React, { Component } from 'react'
import { connect } from 'react-redux'
import YouthMinisterForm from './youthMinisterForm'
import { Modules } from '../../../stores/app.modules'
import { fetchYouthMinisters } from '../../../stores/youth-ministers/youthMinisters.actions'
import { NavLink } from 'react-router-dom'

const mapStateToProps = state => ({
  youthMinisters: state[Modules.YOUTH_MINISTERS].list
})
const mapDispatchToProps = dispatch => ({
  fetchYouthMinisters: () => dispatch(fetchYouthMinisters())
})

class YouthMinisters extends Component {
  componentDidMount() {
    const { fetchYouthMinisters } = this.props
    fetchYouthMinisters()
  }

  render() {
    const { youthMinisters } = this.props

    return (
      <div className='youth ministers'>
        <div className='container pt-4 pb-4'>
          <h2 className='text-center'>Edit Youth Ministers</h2>
          <div className='row pt-3 pb-3 text-center'>
            <div className='col-6'>
              <NavLink className='c-light' to='/about'>About <i className='fa fa-external-link'/></NavLink>
            </div>
            <div className='col-6'>
              <NavLink className='c-light' to='/connect'>Connect <i className='fa fa-external-link'/></NavLink>
            </div>
          </div>
          <div className='row'>
            { youthMinisters.map((youthMinister, i) => (
              <div className='col-lg-6 pb-3' key={i}>
                <YouthMinisterForm youthMinister={youthMinister} />
              </div>
            )) }
            <div className='col-lg-6'>
              <YouthMinisterForm />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(YouthMinisters)
