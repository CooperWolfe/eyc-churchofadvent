import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import YouthMinisterFactory from '../../../model/factories/youth-minister.factory'
import {
  deleteYouthMinister,
  storeYouthMinister,
  updateYouthMinister
} from '../../../stores/youth-ministers/youthMinisters.actions'
import { Modules } from '../../../stores/app.modules'
import Loader from '../../shared/loader'
import logService from '../../../services/log.service'
import FileForm from '../../../helpers/fileForm'
import { deleteFile, uploadFile } from '../../../stores/file/file.actions'
import YouthMinister from '../../../model/youthMinister.model'

const mapStateToProps = state => ({
  loading: state[Modules.YOUTH_MINISTERS].loading,
  uploading: state[Modules.FILE].loading
})
const mapDispatchToProps = dispatch => ({
  addYouthMinister: youthMinister => dispatch(storeYouthMinister(youthMinister)),
  updateYouthMinister: youthMinister => dispatch(updateYouthMinister(youthMinister)),
  deleteYouthMinister: id => dispatch(deleteYouthMinister(id)),
  uploadFile: file => dispatch(uploadFile(file)),
  deleteFile: url => dispatch(deleteFile(url))
})

class YouthMinisterForm extends FileForm {
  static propTypes = {
    youthMinister: PropTypes.object
  }
  static defaultProps = {
    youthMinister: null
  }
  static getDerivedStateFromProps(props, state) {
    const updateObject = {}

    if (props.youthMinister && state.data && props.youthMinister._id !== state.data._id) {
      updateObject.data = props.youthMinister
    }

    return updateObject
  }

  state = {
    ...FileForm.defaultState,
    data: YouthMinisterFactory.create(),
    isNew: false,
    waiting: false
  }

  componentDidMount() {
    const { youthMinister } = this.props

    if (!youthMinister) {
      this.setState({ isNew: true })
    }
    else {
      this.setState({ data: youthMinister })
    }
  }

  clearForm = () => {
    this.setState({ data: YouthMinisterFactory.create() })
  }

  renderEmail = () => {
    return (
      <li className='list-group-item text-center'>
        <i className='float-left fa fa-envelope'/>
        { this.renderLabellessField('Email', 'email', 'text', 'w-75 d-inline-block text-center') }
        <span className=''>@churchofadvent.org</span>
      </li>
    )
  }

  renderPhone = (reason, name) => {
    return (
      <li className='list-group-item text-center'>
        <i className='float-left fa fa-phone'/>
        { this.renderLabellessField(`${reason} (numbers only)`, name, 'text', 'w-75 d-inline-block text-center') }
        <span className=''>({ reason })</span>
      </li>
    )
  }

  renderButtons() {
    const { loading } = this.props
    const { waiting, isNew } = this.state

    if (waiting && loading) {
      return <Loader/>
    }
    return (
      <div className='btn-group'>
        { isNew && <button className='btn btn-dark' type='button' onDoubleClick={this.clearForm}>Clear<br/><small>(double-click)</small></button> }
        { !isNew && <button className='btn btn-danger' type='button' onDoubleClick={this.handleDelete}>Delete<br/><small>(double-click)</small></button> }
        { this.renderButton('submit', 'success', isNew ? 'Save' : 'Update') }
      </div>
    )
  }

  render() {
    const { data } = this.state
    const { imageUrl } = data
    const { uploading } = this.props

    return (
      <div className='youth minister form'>
        <div className='card'>
          <form onSubmit={this.handleSubmit}>
            <div className='card-img-top container text-center'>
              {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
              <img src={imageUrl} alt='Upload an image' className='mw-100' style={{ maxHeight: 340 }}/>
              <br/>
              { uploading !== 0 && <button className='btn btn-success d-block m-auto'><Loader/></button> }
              { uploading === 0 && this.renderFileUpload('Upload', 'imageUrl', `image-${data._id}`, 'success', 'image/*', 'mt-2') }
            </div>
            <div className='card-body'>
              <h5 className='card-title text-center'>
                <div className='row'>
                  <div className='col-6'>
                    { this.renderLabellessField('First Name', 'firstName', 'text') }
                  </div>
                  <div className='col-6'>
                    { this.renderLabellessField('Last Name', 'lastName', 'text') }
                  </div>
                </div>
                <div className='bio'>
                  { this.renderLabellessTextArea('Bio', 'bio', '3') }
                </div>
              </h5>
              <ul className='list-group list-group-flush' style={{ marginBottom: '-.5rem' }}>
                { this.renderEmail() }
                { this.renderPhone('Office', 'officePhone') }
                { this.renderPhone('Cell', 'cellPhone') }
              </ul>
            </div>
            <div className='card-footer text-center'>
              { this.renderButtons() }
            </div>
          </form>
        </div>
      </div>
    )
  }

  doSubmit = () => {
    const { addYouthMinister, updateYouthMinister } = this.props
    let { isNew, data } = this.state

    data = YouthMinister.fromObject(data)

    this.setState({ waiting: true })

    const action = isNew
      ? addYouthMinister(data).then(() => this.clearForm())
      : updateYouthMinister(data)

    action.then(() => {
      this.setState({ waiting: false })
      logService.alert('Saved successfully')
    })
  }

  doChange = e => {
    if (e.currentTarget.name !== 'imageUrl') return
    this.doSubmit()
  }

  handleDelete = () => {
    const { deleteYouthMinister, deleteFile } = this.props
    const { isNew, data } = this.state
    if (data.imageUrl) deleteFile(data.imageUrl)
    if (isNew) return;
    deleteYouthMinister(data.id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(YouthMinisterForm)
