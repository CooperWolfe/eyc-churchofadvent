import React from 'react'
import { connect } from 'react-redux'
import Form from '../../helpers/form'
import Joi from 'joi-browser'
import { login } from '../../stores/auth/auth.actions'
import history from '../../services/history.service'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  login: (email, password) => dispatch(login(email, password))
})

class Login extends Form {
  state = {
    ...Form.defaultState,
    data: {
      email: '',
      password: ''
    }
  }
  schema = {
    email: Joi.string().email().required(),
    password: Joi.string().required()
  }

  doSubmit = () => {
    const { login } = this.props
    const { email, password } = this.state.data
    login(email, password).then(() => history.push('/'))
  }

  render() {
    return (
      <div className='login'>
        <div className='container pt-4 pb-4'>
          <div className='row'>
            <div className='col-md-3'/>
            <div className='col-md-6'>
              <form onSubmit={this.handleSubmit} className='text-center'>
                { this.renderField('Email', 'email', 'email', 'text-center') }
                { this.renderField('Password', 'password', 'password', 'text-center') }
                { this.renderButton('submit', 'success', 'Login') }
              </form>
            </div>
            <div className='col-md-3'/>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
