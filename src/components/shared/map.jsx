import React, {Component} from 'react'
import ReactMapGL from 'react-map-gl'

export default class Map extends Component {
  state = {
    viewport: {
      width: 400,
      height: 400,
      latitude: 37.7577,
      longitude: -122.4376,
      zoom: 8
    }
  }

  render() {
    return (
      <ReactMapGL {...this.state.viewport}
                  onViewportChange={(viewport) => this.setState({viewport})}
                  mapboxApiAccessToken='pk.eyJ1IjoiY29vcGVyd29sZmUtd3dkIiwiYSI6ImNrMWs0ODdmajBhN3EzYnBlYThteXA0a2MifQ.5R_HKfbl6YsVecVqCJOHIQ'/>
    );
  }
}
