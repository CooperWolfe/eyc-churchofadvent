import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Parallax as ReactParallax } from 'react-parallax'

const propTypes = {
  imageUrl: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired
}
const defaultProps = {}

class Parallax extends Component {
  render() {
    const { imageUrl, alt, style, children, bgImageStyle } = this.props

    return (
      <ReactParallax
        blur={0}
        bgImage={imageUrl}
        bgImageAlt={alt}
        bgImageStyle={bgImageStyle || {}}
        strength={200}
      >
        <div style={style}>
          { children }
        </div>
      </ReactParallax>
    )
  }
}

Parallax.propTypes = propTypes
Parallax.defaultProps = defaultProps

export default Parallax
