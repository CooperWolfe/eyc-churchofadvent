import React, { Component } from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  style: PropTypes.object,
  className: PropTypes.string
}
const defaultProps = {
  style: {},
  className: ''
}

const spin = {
  animation: 'spin 1s linear infinite, fade 1.33333s linear infinite'
}

class Loader extends Component {
  render() {
    let { style, className, ...props } = this.props
    style = { ...style, ...spin }

    const nonFontawesomeInputClasses = className.split(' ').filter(clazz => clazz.substr(0, 2) !== 'fa').join(' ')
    className = `fa fa-circle-o-notch ${nonFontawesomeInputClasses}`

    return <i style={style} className={className} {...props} />
  }
}

Loader.propTypes = propTypes
Loader.defaultProps = defaultProps

export default Loader
