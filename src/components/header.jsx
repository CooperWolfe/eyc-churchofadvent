import React, { Component } from 'react'
import Slider from 'react-animated-slider'
import './header.css'
import 'react-animated-slider/build/horizontal.css'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Modules } from '../stores/app.modules'
import { fetchSlides } from '../stores/slides/slides.actions'

const mapStateToProps = state => ({
  slides: state[Modules.SLIDES].list
})
const mapDispatchToProps = dispatch => ({
  fetchSlides: () => dispatch(fetchSlides())
})

class Header extends Component {
  componentDidMount() {
    const { fetchSlides } = this.props
    fetchSlides()
  }

  renderArrow = direction => {
    const { slides } = this.props
    if (slides.length <= 1) return null
    return <i className={`fa fa-angle-${direction} arrow-${direction}`}/>
  }

  render() {
    const { slides } = this.props

    return (
      <div className='header'>
        <Slider className='slider-wrapper'
                previousButton={this.renderArrow('left')}
                nextButton={this.renderArrow('right')}
                infinite
                autoplay={4000}>
          { slides.map((slide, i) => (
            <div key={i} className='slider-content' style={{ background: `url('${slide.imageUrl}') no-repeat center center` }}>
              <div className='inner'>
                <h1>{slide.title}</h1>
                { slide.buttonText && <NavLink to={slide.buttonUrl} className='btn mt-5 btn-success cursor-pointer'>{slide.buttonText}</NavLink> }
              </div>
            </div>
          )) }
        </Slider>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
