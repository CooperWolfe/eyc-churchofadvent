import React, { Component } from 'react'
import { connect } from 'react-redux'
import EventLink from './event-link'
import { Calendar } from 'react-big-calendar'
import localizer from '../../services/calendarLocalizer.service'
import { fetchEvents } from '../../stores/event/event.actions'
import { Modules } from '../../stores/app.modules'
import './events.css'
import { today } from '../../utilities/date.utility'
import Loader from '../shared/loader'
import { NavLink } from 'react-router-dom'
import { toDashCase } from '../../utilities/string.utility'
import Legend from './legend'

const mapStateToProps = state => ({
  loading: state[Modules.EVENTS].loading > 0,
  events: state[Modules.EVENTS].list || []
})
const mapDispatchToProps = dispatch => ({
  fetchEvents: () => dispatch(fetchEvents())
})

class Events extends Component {
  componentDidMount() {
    const { fetchEvents } = this.props
    fetchEvents()
  }

  renderUpcomingEvent = event => {
    return (
      <div className='upcoming-event text-center'>
        <h6 className='text-muted'>
          <NavLink to={`/events/${toDashCase(event.title)}?id=${event.id}`}
                   className='c-accent font-weight-bold'
                   style={{ fontSize: '1.1rem' }}>
            { event.title }
          </NavLink>
          &nbsp;-&nbsp;{ event.dateTimeSummary }
        </h6>
        <p className='p-2 m-0'>{ event.description }</p>
      </div>
    )
  }
  render() {
    const { events, loading } = this.props

    // Calculate default view
    const defaultView = window.innerWidth > 800 ? 'month' : 'agenda'

    // Define event rendering
    const components = {
      agenda: {
        event: EventLink
      },
      month: {
        event: props => <EventLink {...props} color='light'/>
      }
    }

    // Calculate upcoming events
    const upcomingEvents = events.filter(event => event.start > today()).slice(0, 5)

    return (
      <div className='calendar'>
        <h1 className='text-center p-4'>UPCOMING Events</h1>
        <div className='container'>
          { loading ? <div className='text-center'><Loader className='c-light' style={{ fontSize: '2rem' }}/></div> :
            <ul className='list-group'>
              {upcomingEvents.map((event, i) => (
                <li key={i} className='list-group-item'>{this.renderUpcomingEvent(event)}</li>
              ))}
            </ul> }
        </div>
        <h1 className='text-center p-4'>CALENDAR</h1>
        <div className='container rounded bg-light p-3'>
          <Calendar localizer={localizer}
                    events={events}
                    defaultView={defaultView}
                    views={['agenda', 'month']}
                    eventPropGetter={event => ({ className: Legend.ClassesForType[event.type] })}
                    components={components}
                    onNavigate={this.handleNavigate}
                    popup/>
          <Legend/>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Events)
