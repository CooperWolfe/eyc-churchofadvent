import React, { Component } from 'react'
import { connect } from 'react-redux'
import './legend.css'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class Legend extends Component {
  state = {}

  static ClassesForType = {
    'middle-school': 'bg-primary c-light',
    'high-school': 'bg-accent c-light',
    'college': 'bg-danger c-light',
    'all': 'default'
  }

  renderEntry = (entry, name) => {
    return (
      <div className='entry m-auto w-fit pt-1 pb-1'>
        <div className={`palette ${Legend.ClassesForType[entry]}`}/>&nbsp;
        <div className='name'>{ name }</div>
      </div>
    )
  }
  render() {
    return (
      <div className='legend p-2'>
        <div className='container'>
          <div className='row'>
            <div className='col-6'>{ this.renderEntry('middle-school', 'Middle') }</div>
            <div className='col-6'>{ this.renderEntry('high-school', 'High') }</div>
          </div>
          <div className='row'>
            <div className='col-6'>{ this.renderEntry('college', 'College') }</div>
            <div className='col-6'>{ this.renderEntry('all', 'All Students') }</div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Legend)
