import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { toDashCase } from '../../utilities/string.utility'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class EventLink extends Component {
  static propTypes = {
    title: PropTypes.string,
    event: PropTypes.object
  }
  static defaultProps = {
    color: 'accent'
  }

  state = {}

  render() {
    const { title, event, color } = this.props

    const url = toDashCase(title)

    return (
      <NavLink to={`/event/${url}?id=${event.id}`} className={`c-${color}`}>{ title }</NavLink>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EventLink)
