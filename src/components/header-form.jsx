import React, { Component } from 'react'
import { connect } from 'react-redux'
import Slider from 'react-animated-slider'
import './header-form.css'
import { deleteSlide, fetchSlides, postSlide, storeSlide } from '../stores/slides/slides.actions'
import Slide from '../model/slide.model'
import { Modules } from '../stores/app.modules'
import logService from '../services/log.service'
import SlideFactory from '../model/factories/slide.factory'
import uuid from 'uuid/v4'
import { uploadFile, deleteFile } from '../stores/file/file.actions'

const mapStateToProps = state => ({
  slides: state[Modules.SLIDES].list
})
const mapDispatchToProps = dispatch => ({
  fetchSlides: () => dispatch(fetchSlides()),
  updateSlide: slide => dispatch(postSlide(slide)),
  addSlide: slide => dispatch(storeSlide(slide)),
  deleteSlide: id => dispatch(deleteSlide(id)),
  uploadFile: file => dispatch(uploadFile(file)),
  deleteFile: file => dispatch(deleteFile(file))
})

class HeaderForm extends Component {
  state = {
    slides: [],
    newSlide: SlideFactory.create(),
    hasLink: true
  }

  componentDidMount() {
    const { fetchSlides } = this.props
    fetchSlides()
  }

  static getDerivedStateFromProps(props, state) {
    if (props.slides.length !== state.slides.length) {
      return { slides: props.slides.map(slide => Slide.fromObject(slide)) }
    }
    return null
  }

  renderArrow = direction => {
    const { slides } = this.props
    if (slides.length <= 1) return null
    return <i className={`fa fa-angle-${direction} arrow-${direction}`}/>
  }

  handleChange = (e, slide, index) => {
    const { slides } = this.state
    this.setState({
      slides: [
        ...slides.slice(0, index),
        Slide.fromObject({ ...slide, [e.currentTarget.name]: e.currentTarget.value }),
        ...slides.slice(index + 1)
      ]
    })
  }
  handleNewChange = e => {
    const { newSlide } = this.state
    this.setState({
      newSlide: Slide.fromObject({ ...newSlide, [e.currentTarget.name]: e.currentTarget.value })
    })
  }
  handleSave = slide => {
    const { updateSlide } = this.props
    updateSlide(slide).then(() => logService.alert('Saved successfully'))
  }
  handleAdd = (e, slide) => {
    const { uploadFile, addSlide } = this.props

    // Exit early if no file
    let file = e.currentTarget.files[0]
    if (!file) return

    // Make unique file name
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1)
    file = new File([file], `${uuid()}.${extension}`, { type: file.type })

    this.handleClear()

    // Upload file
    const name = e.currentTarget.name
    uploadFile(file).then(url => {
      slide[name] = url

      // Add slide
      addSlide(slide).then(() => {
        logService.alert('Added slide successfully')
        this.handleClear()
      })
    })
  }
  handleClear = () => {
    this.setState({ newSlide: SlideFactory.create() })
  }
  handleDelete = id => {
    const { deleteFile, deleteSlide } = this.props
    const { slides } = this.props

    // Get slide
    const slide = slides.filter(slide => slide._id === id)[0]
    if (!slide) return

    // Delete file (if necessary) first
    if (slide.imageUrl) {
      deleteFile(slide.imageUrl).then(() => {
        deleteSlide(id)
      })
    }
    else {
      deleteSlide(id)
    }
  }

  render() {
    const { slides, newSlide } = this.state

    return (
      <div className='header'>
        <Slider className='slider-wrapper'
                previousButton={this.renderArrow('left')}
                nextButton={this.renderArrow('right')}
                duration={1}>
          { slides.map((slide, i) => (
            <div key={i} className='slider-content' style={{ background: `url('${slide.imageUrl}') no-repeat center center` }}>
              <div className='inner'>
                <input className='slide-title form-control w-auto m-auto'
                       name='title'
                       value={slide.title}
                       onChange={e => this.handleChange(e, slide, i)} />
                <div className='slide-button btn mt-3 btn-success cursor-pointer'>
                  <input className='slide-button-text form-control text-center'
                         name='buttonText'
                         value={slide.buttonText}
                         onChange={e => this.handleChange(e, slide, i)} />
                  <small>Navigates to:</small>
                  <input className='slide-button-url form-control text-center'
                         name='buttonUrl'
                         value={slide.buttonUrl}
                         onChange={e => this.handleChange(e, slide, i)}/>
                </div>
                <div className='btn-group mt-3'>
                  <button className='btn btn-success' onClick={() => this.handleSave(slide)}>Save</button>
                  <button className='btn btn-danger' onClick={() => this.handleDelete(slide._id)}>Delete</button>
                </div>
              </div>
            </div>
          )) }
          <div className='slider-content' style={{ background: `url('${newSlide.imageUrl}') no-repeat center center` }}>
            <div className='inner'>
              <input className='slide-title form-control w-auto m-auto'
                     name='title'
                     value={newSlide.title}
                     onChange={this.handleNewChange} />
              <div className='slide-button btn mt-3 btn-success cursor-pointer'>
                <input className='slide-button-text form-control text-center'
                       name='buttonText'
                       value={newSlide.buttonText}
                       onChange={this.handleNewChange} />
                <small>Navigates to:</small>
                <input className='slide-button-url form-control text-center'
                       name='buttonUrl'
                       value={newSlide.buttonUrl}
                       onChange={this.handleNewChange}/>
              </div>
              <div className='btn-group mt-3'>
                <label htmlFor='image' className='btn btn-success m-0'>Upload & Add</label>
                <button className='btn btn-dark' onClick={this.handleClear}>Clear</button>
                <input type='file' hidden accept='image/*' id='image' name='imageUrl' onChange={e => this.handleAdd(e, newSlide)}/>
              </div>
            </div>
          </div>
        </Slider>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderForm)
