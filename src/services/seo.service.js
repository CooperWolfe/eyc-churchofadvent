const head = document.getElementsByTagName('head')[0]
if (!head) throw new Error('could not find head')

class SeoState {
  title = ''
  metas = {}
  links = {}
  ldJson = {}

  setTitle(title) {
    if (!title) return
    if (typeof title !== 'string') return
    this.title = title
  }
  setMeta(name, content) {
    if (!name || !content) return
    if (typeof name !== 'string' || typeof content !== 'string') return
    this.metas[name] = content
  }
  setLink(rel, href) {
    if (!rel || !href) return
    if (typeof rel !== 'string' || typeof href !== 'string') return
    this.links[rel] = href
  }
  setLdJson(key, ldJson) {
    if (!ldJson) return
    if (typeof ldJson !== 'object') return
    this.ldJson[key] = ldJson
  }
}

class SeoManager {
  stack = []
  id = -1

  getState() {
    const state = new SeoState()
    const stack = this.stack.map(stackObject => stackObject.state)
    for (const current of stack) {
      if (current.title) state.setTitle(current.title)
      state.metas = {
        ...state.metas,
        ...current.metas
      }
      state.links = {
        ...state.links,
        ...current.links
      }
      state.ldJson = {
        ...state.ldJson,
        ...current.ldJson
      }
    }
    return state
  }
  pushState() {
    const state = new SeoState()
    this.stack.push({ id: ++this.id, state })
    return {
      state,
      id: this.id
    }
  }
  renderSeo(state) {
    state = state || this.getState()
    if (state.title) document.title = state.title
    for (const name of Object.keys(state.metas))
      SeoManager.setHeadTag('meta', 'name', name, 'content', state.metas[name])
    for (const rel of Object.keys(state.links))
      SeoManager.setHeadTag('link', 'rel', rel, 'href', state.links[rel])
    for (const key of Object.keys(state.ldJson))
      SeoManager.setLdJson(key, state.ldJson[key])
  }
  popState(id) {
    let poppedState = this.stack.filter(stackObject => stackObject.id === id)[0]
    const index = this.stack.indexOf(poppedState)
    poppedState = poppedState.state
    this.stack = [
      ...this.stack.slice(0, index),
      ...this.stack.slice(index + 1)
    ]
    const state = this.getState()
    if (!state.title) throw new Error('Your page must have a title')
    for (const name of Object.keys(poppedState.metas))
      if (!state.metas[name])
        SeoManager.removeHeadTag('meta', 'name', name)
    for (const rel of Object.keys(poppedState.links))
      if (!state.links[rel])
        SeoManager.removeHeadTag('link', 'rel', rel)
    for (const key of Object.keys(poppedState.ldJson))
      if (!state.ldJson[key])
        SeoManager.removeHeadTag('script', 'key', key)
  }

  static setHeadTag(tagName, keyAttr, keyAttrVal, valAttr, valAttrVal) {
    let tag = Array.from(document.getElementsByTagName(tagName))
      .filter(el => el.getAttribute(keyAttr) === keyAttrVal)[0]
    if (tag) return tag.setAttribute(valAttr, valAttrVal)
    tag = document.createElement(tagName)
    tag.setAttribute(keyAttr, keyAttrVal)
    tag.setAttribute(valAttr, valAttrVal)
    head.appendChild(tag)
  }
  static setLdJson(key, { context, type, ...rest }) {
    const ldJson = { '@context': context, '@type': type, ...rest }
    const content = JSON.stringify(ldJson)
    let script = Array.from(document.getElementsByTagName('script'))
      .filter(el => el.getAttribute('key') === key)[0]
    if (script) return script.innerHTML = content
    script = document.createElement('script')
    script.setAttribute('type', 'application/ld+json')
    script.setAttribute('key', key)
    script.innerHTML = content
    head.appendChild(script)
  }
  static removeHeadTag(tagName, keyAttr, keyAttrVal) {
    Array.from(document.getElementsByTagName(tagName))
      .filter(tag => tag.getAttribute(keyAttr) === keyAttrVal)
      .forEach(tag => tag.remove())
  }
}

const seoService = new SeoManager()

export default seoService
