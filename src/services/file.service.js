import firebase from './firebase.service'

export function uploadFile(file, path) {
  return new Promise((resolve, reject) => {
    firebase.storage().ref(`${path}/${file.name}`).put(file)
      .then(snapshot => snapshot.ref.getDownloadURL().then(resolve).catch(reject))
      .catch(reject)
  })
}

export function deleteFile(url) {
  return firebase.storage().refFromURL(url).delete()
}
