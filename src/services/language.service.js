import Observable from '../patterns/observer/observable'

/**
 * Service that keeps track of the system language
 * Use for localization
 */
class LanguageService {
  languageObservable = new Observable('en')

  /**
   * Sets the language code for the service
   * @param {string} languageCode - the current language code
   */
  setLanguage(languageCode) {
    this.languageObservable.pushChange(languageCode)
  }

  /**
   * Registers the callback to language changes
   * @param {function} callback - what should happen when the language changes
   * @returns {Subscription} - A subscription to language changes; remember to unsubscribe
   */
  register(callback) {
    return this.languageObservable.register(callback)
  }
}

const languageService = new LanguageService()

export default languageService
