import http from 'axios'
import logService from './log.service'

const _401Urls = []

// Authorization error
http.interceptors.response.use(null, err => {
  if (err.response && err.response.status === 401 && !_401Urls.includes(err.request.responseURL))
    localStorage.removeItem('token')
  return Promise.reject(err)
})
// Unexpected errors
http.interceptors.response.use(null, err => {
  const errorExpected = err.response && err.response.status >= 400 && err.response.status < 500

  if (!errorExpected) {
    logService.log(err)
    logService.alertError(err.message || 'An unexpected error occurred.')
    // logService.logSentry(err)
  }

  return Promise.reject(err)
})

// Authentication
http.interceptors.request.use(req => {
  const token = localStorage.getItem('token')
  if (!token) return req
  req.headers['Authorization'] = `Bearer ${token}`
  return req
})
