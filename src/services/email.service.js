import * as AWS from 'aws-sdk'

AWS.config.region = 'us-east-1';
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: 'us-east-1:04c512c6-ab49-4944-a753-c8e284436adc',
});

function createEmail(to, from, subject, message) {
  return {
    Destination: {
      ToAddresses: typeof to === 'string' ? [to] : to
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: `<p>${message.replace(/[\r\n]/g, '<br>')}</p>`
        },
        Text: {
          Charset: 'UTF-8',
          Data: message
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: subject
      }
    },
    Source: from
  }
}

function sendEmail(to, from, subject, message) {
  const email = createEmail(to, from, subject, message)
  return new AWS.SES()
    .sendEmail(email)
    .promise()
}

const emailService = {
  sendEmail
}

export default emailService
