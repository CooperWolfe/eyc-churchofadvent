import firebase from 'firebase/app'

import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

const config = {
  apiKey: 'AIzaSyD16OJ9OvUsYKII4OFWKCPMucEyECmK4KM',
  authDomain: 'eyc-churchofadvent.firebaseapp.com',
  databaseURL: "https://eyc-churchofadvent.firebaseio.com",
  projectId: "eyc-churchofadvent",
  storageBucket: "eyc-churchofadvent.appspot.com"
}
firebase.initializeApp(config)
firebase.firestore()
firebase.auth()

export default firebase
