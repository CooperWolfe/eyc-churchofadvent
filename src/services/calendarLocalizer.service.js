// import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import { momentLocalizer } from 'react-big-calendar'

const localizer = momentLocalizer(moment)

export default localizer
