import React from 'react'
import ReactDOM from 'react-dom'
import App from './app.jsx'
import { Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './stores/app.store'
import history from './services/history.service'
import * as serviceWorker from './serviceWorker'
import './services/firebase.service'
import './services/axios.service'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/font-awesome/css/font-awesome.min.css'
import '../node_modules/react-toastify/dist/ReactToastify.min.css'
import '../node_modules/react-big-calendar/lib/css/react-big-calendar.css'
import '../node_modules/react-datetime/css/react-datetime.css'
import '../node_modules/react-image-gallery/styles/css/image-gallery-no-icon.css'
import './index.css'

ReactDOM.render(
  <Provider store={store}><Router history={history}><App /></Router></Provider>,
  document.getElementById('root')
)

// Change to register on final production
serviceWorker.unregister()
