import firebase from '../../services/firebase.service'
import AsyncActions from './async-actions'

/** Base for all actions that fetch a single data point */
export default class DataActions extends AsyncActions {
  _docRef
  $collectionId
  $docId

  $getDocRef() {
    if (!this._docRef)
      this._docRef = firebase.firestore().collection(this.$collectionId).doc(this.$docId)
    return this._docRef
  }
  $convertDocToData(documentSnapshot) {
    return {
      id: documentSnapshot.id,
      ...documentSnapshot.data()
    }
  }

  // Strategies
  $optimistic(before, promise, dispatch, getState, onError = undefined) {
    dispatch(this.request())
    const previousData = getState()[this.$module].data
    before && before()
    return promise
      .then(() => dispatch(this.receive()))
      .catch(err => {
        dispatch(this.set(previousData))
        return dispatch(this.error(err))
      })
  }

  // Synchronous
  set = data => ({
    type: DataActions.SET,
    module: this.$module,
    data
  })

  // Asynchronous
  fetch = () => {
    return dispatch => this.$pessimistic(
      this.$getDocRef().get()
        .then(this.$convertDocToData)
        .then(data => dispatch(this.set(data))),
      dispatch
    )
  }

  // Static
  static SET = 'SET'
}
