import Actions from './actions'

export default class AsyncActions extends Actions {

  $pessimistic(promise, dispatch, onError, onSuccess) {
    dispatch(this.request())
    return promise
      .then(res => {
        onSuccess && onSuccess(res)
        return res
      })
      .then(res => {
        dispatch(this.receive())
        return res
      })
      .catch(error => {
        onError && onError(error)
        dispatch(this.error(error))
      })
  }

  $optimistic(before, promise, dispatch, getState, onError) { throw new Error('$optimistic not implemented') }

  $convertDocToData(documentSnapshot) { throw new Error('$convertDocToData not implemented') }
}
