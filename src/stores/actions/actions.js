/** Base for all actions */
export default class Actions {
  /**
   * What module the actions are in
   * @protected Module
   */
  $module

  /** Causes module to begin loading for a task */
  request = () => ({
    type: Actions.REQUEST,
    module: this.$module
  })
  /** Stops loading a task and resets error */
  receive = () => ({
    type: Actions.RECEIVE,
    module: this.$module
  })
  /**
   * Stops loading a task and sets error
   * @param {Error} error - the error that has occurred
   */
  error = error => ({
    type: Actions.ERROR,
    module: this.$module,
    error
  })

  static REQUEST = 'REQUEST'
  static RECEIVE = 'RECEIVE'
  static ERROR = 'ERROR'
}
