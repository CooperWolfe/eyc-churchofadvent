import DataActions from './data-actions'

/** Base for all actions that fetch a single model data point */
export default class ModelActions extends DataActions {
  /**
   * @inheritDoc
   * @returns {*}
   */
  $convertDocToData(documentSnapshot) {
    throw '$convertDocToData not implemented'
  }

  // Synchronous
  /**
   * Updates data
   * @param {Model} data - the data to update
   */
  update = data => ({
    type: ModelActions.UPDATE,
    module: this.$module,
    data
  })

  // Async
  /**
   * Updates data persistently
   * @param {Model} data - the data to update on the backend
   */
  post = data => (dispatch, getState) => this.$optimistic(
    () => dispatch(this.update(data)),
    this.$getDocRef().update(data.toPersistenceObject()),
    dispatch,
    getState
  )

  // Constants
  static UPDATE = 'UPDATE'
}
