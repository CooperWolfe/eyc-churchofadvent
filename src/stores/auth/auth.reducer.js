import Reducer from '../reducers/reducer'
import { Modules } from '../app.modules'
import firebase from '../../services/firebase.service'
import AuthActions from './auth.actions'

/** Reducer for authentication */
class AuthReducer extends Reducer {
  /** @inheritDoc */
  $module = Modules.AUTH

  /** Authenticate reducer */
  authenticate = () => ({ authenticated: true })
  /** Unauthenticate reducer */
  unauthenticate = () => ({ authenticated: false })

  /** @inheritDoc */
  $getHandlerMap() {
    return {
      ...super.$getHandlerMap(),
      [AuthActions.AUTHENTICATE]: this.authenticate,
      [AuthActions.UNAUTHENTICATE]: this.unauthenticate
    }
  }
  /** @inheritDoc */
  getInitialState() {
    return {
      ...super.getInitialState(),
      authenticated: !!firebase.auth().currentUser
    }
  }
}

/**
 * A reducer for authentication
 * @type {AuthReducer}
 */
const authReducer = new AuthReducer()

export default authReducer
