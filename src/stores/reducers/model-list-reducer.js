import Reducer from './reducer'
import Actions from '../actions/model-list-actions'

/** The base for all reducers with a list of model objects */
export default class ModelListReducer extends Reducer {

  /** Set list reducer */
  setList = (state, { list }) => ({ ...state, list })
  /** Select reducer */
  select = (state, { id }) => {
    const selected = state.list.filter(item => item.id === id)[0]
    return { ...state, selected }
  }
  /** Deselect reducer */
  deselect = state => ({ ...state, selected: null })
  /** Append reducer */
  append = (state, { item }) => ({ ...state, list: [...state.list, item] })
  /** Prepend reducer */
  prepend = (state, { item }) => ({ ...state, list: [item, ...state.list] })
  /** Update reducer */
  update = (state, { item }) => {
    const existing = state.list.filter(el => el.id === item.id)[0]
    const index = state.list.indexOf(existing)
    return index === -1 ? state : {
      ...state,
      list: [
        ...state.list.slice(0, index),
        state.list[index].update(item),
        ...state.list.slice(index + 1)
      ]
    }
  }
  /** Remove reducer */
  remove = (state, { id }) => {
    const existing = state.list.filter(item => item.id === id)[0]
    const index = state.list.indexOf(existing)
    return {
      ...state,
      list: [
        ...state.list.slice(0, index),
        ...state.list.slice(index + 1)
      ]
    }
  }


  /** @inheritDoc */
  $getHandlerMap() {
    return {
      ...super.$getHandlerMap(),
      [Actions.SET_LIST]: this.setList,
      [Actions.SELECT]: this.select,
      [Actions.DESELECT]: this.deselect,
      [Actions.APPEND]: this.append,
      [Actions.PREPEND]: this.prepend,
      [Actions.UPDATE]: this.update,
      [Actions.REMOVE]: this.remove
    }
  }
  /** @inheritDoc */
  getInitialState() {
    return {
      ...super.getInitialState(),
      list: [],
      selected: null
    }
  }
}
