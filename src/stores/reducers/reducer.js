import Actions from '../actions/actions'

/** Base for all reducers */
export default class Reducer {
  /**
   * What module the reducer is in
   * @protected
   */
  $module

  /** Request reducer */
  request = state => ({ ...state, loading: state.loading + 1 })
  /** Receive reducer */
  receive = state => ({ ...state, loading: state.loading - 1, error: null })
  /** Error reducer */
  error = (state, { error }) => ({ ...state, loading: state.loading - 1, error })

  /**
   * A map from action constants to their reducer functions
   * @protected
   * @returns object
   */
  $getHandlerMap() {
    return {
      [Actions.REQUEST]: this.request,
      [Actions.RECEIVE]: this.receive,
      [Actions.ERROR]: this.error
    }
  }

  /**
   * The initial state of the reducer
   * @returns object
   */
  getInitialState() {
    return {
      loading: 0,
      error: null
    }
  }

  /**
   * Reducer function
   * @param {object} state - the state before reduction
   * @param {object} action - action information
   * @returns {object} the state after reduction
   */
  reduce(state = this.getInitialState(), action) {
    if (action.module !== this.$module) throw new Error('Wrong module')

    const handlerMap = this.$getHandlerMap()
    const handler = handlerMap[action.type]
    if (!handler) return state

    return {
      ...state,
      [this.$module]: handler(state[this.$module], action)
    }
  }
}
