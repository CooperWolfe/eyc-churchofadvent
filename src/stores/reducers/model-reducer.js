import DataReducer from './data-reducer'
import Actions from '../actions/model-actions'

/** Base for all reducers with a model data point */
export default class ModelReducer extends DataReducer {
  /** Update reducer */
  update = (state, { data }) => {
    return ({
      ...state,
      data: state.data.update(data)
    })
  }

  /** @inheritDoc */
  $getHandlerMap() {
    return {
      ...super.$getHandlerMap(),
      [Actions.UPDATE]: this.update
    }
  }
}
