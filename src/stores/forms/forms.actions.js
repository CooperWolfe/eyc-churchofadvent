import ModelListActions from '../actions/model-list-actions'
import { Modules } from '../app.modules'
import Form from '../../model/form.model'

class FormActions extends ModelListActions {
  $collectionId = 'forms'
  $module = Modules.FORMS

  $convertDocToData(documentSnapshot) {
    return Form.fromDocumentSnapshot(documentSnapshot)
  }
}

const formActions = new FormActions()

export const fetchForms = formActions.fetch
export const storeForm = formActions.store
export const postForm = formActions.post
export const deleteForm = formActions.delete
