import ModelListReducer from '../reducers/model-list-reducer'
import { Modules } from '../app.modules'

class FormsReducer extends ModelListReducer {
  $module = Modules.FORMS
}

const formsReducer = new FormsReducer()

export default formsReducer
