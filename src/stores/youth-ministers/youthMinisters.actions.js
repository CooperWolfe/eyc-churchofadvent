import ModelListActions from '../actions/model-list-actions'
import { Modules } from '../app.modules'
import YouthMinister from '../../model/youthMinister.model'

class YouthMinistersActions extends ModelListActions {
  $collectionId = 'youth-ministers'
  $module = Modules.YOUTH_MINISTERS

  $convertDocToData(documentSnapshot) {
    return YouthMinister.fromDocumentSnapshot(documentSnapshot)
  }
}

const youthMinistersActions = new YouthMinistersActions()

export const fetchYouthMinisters = youthMinistersActions.fetch
export const storeYouthMinister = youthMinistersActions.store
export const updateYouthMinister = youthMinistersActions.post
export const deleteYouthMinister = youthMinistersActions.delete
