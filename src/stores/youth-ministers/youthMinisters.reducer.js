import ModelListReducer from '../reducers/model-list-reducer'
import { Modules } from '../app.modules'

class YouthMinistersReducer extends ModelListReducer {
  $module = Modules.YOUTH_MINISTERS
}

const youthMinistersReducer = new YouthMinistersReducer()

export default youthMinistersReducer
