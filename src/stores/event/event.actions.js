import { Modules } from '../app.modules'
import Event from '../../model/event.model'
import ModelListActions from '../actions/model-list-actions'

class EventActions extends ModelListActions {
  $collectionId = 'events'
  $module = Modules.EVENTS

  $convertDocToData(documentSnapshot) {
    return Event.fromDocumentSnapshot(documentSnapshot)
  }
}

const eventActions = new EventActions()

export const fetchEvents = eventActions.fetch
export const storeEvent = eventActions.store
export const postEvent = eventActions.post
export const deleteEvent = eventActions.delete
