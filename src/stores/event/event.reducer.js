import ModelListReducer from '../reducers/model-list-reducer'
import { Modules } from '../app.modules'

class EventReducer extends ModelListReducer {
  $module = Modules.EVENTS

  setList = (state, { list }) => {
    list.sort((lhs, rhs) => lhs.start - rhs.start)
    return { ...state, list }
  }
}

const eventReducer = new EventReducer()

export default eventReducer
