import AsyncActions from '../actions/async-actions'
import { Modules } from '../app.modules'
import http from 'axios'

const token = '20108943900.88924c8.1ebfe77fe4a4486fb3e6d14bcfa857fb'
const apiUrl = 'https://api.instagram.com/v1/users/self'

export default class InstagramActions extends AsyncActions {
  $module = Modules.INSTAGRAM

  // Synchronous
  set = posts => ({
    type: InstagramActions.SET,
    module: this.$module,
    posts
  })

  // Asynchronous
  fetch = () => dispatch => {
    return this.$pessimistic(
      http.get(`${apiUrl}/media/recent`, { params: { access_token: token } })
        .then(res => {
          return res.data.data.splice(0, 10)
        })
        .then(posts => dispatch(this.set(posts))),
      dispatch
    )
  }

  static SET = 'SET'
}

const instagramActions = new InstagramActions()

export const fetchPosts = instagramActions.fetch
