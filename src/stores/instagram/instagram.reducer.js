import Reducer from '../reducers/reducer'
import { Modules } from '../app.modules'
import InstagramActions from './instagram.actions'

class InstagramReducer extends Reducer {
  $module = Modules.INSTAGRAM

  set = (state, { posts }) => ({ ...state, posts })

  $getHandlerMap() {
    return {
      ...super.$getHandlerMap(),
      [InstagramActions.SET]: this.set
    }
  }

  getInitialState() {
    return {
      ...super.getInitialState(),
      posts: []
    }
  }
}

const instagramReducer = new InstagramReducer()

export default instagramReducer
