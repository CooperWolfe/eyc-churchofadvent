import ModelListActions from '../actions/model-list-actions'
import { Modules } from '../app.modules'
import Slide from '../../model/slide.model'

class SlidesActions extends ModelListActions {
  $collectionId = 'slides'
  $module = Modules.SLIDES



  $convertDocToData(documentSnapshot) {
    return Slide.fromDocumentSnapshot(documentSnapshot)
  }
}

const slidesActions = new SlidesActions()

export const fetchSlides = slidesActions.fetch
export const storeSlide = slidesActions.store
export const postSlide = slidesActions.post
export const deleteSlide = slidesActions.delete
