import ModelListReducer from '../reducers/model-list-reducer'
import { Modules } from '../app.modules'

class SlidesReducer extends ModelListReducer {
  $module = Modules.SLIDES

  setList = (state, { list }) => {
    list.sort((lhs, rhs) => lhs.date - rhs.date)
    return { ...state, list }
  }
}

const slidesReducer = new SlidesReducer()

export default slidesReducer
