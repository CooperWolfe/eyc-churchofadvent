import ModelListReducer from '../reducers/model-list-reducer'
import { Modules } from '../app.modules'

class PodcastReducer extends ModelListReducer {
  $module = Modules.PODCASTS
}

const podcastReducer = new PodcastReducer()

export default podcastReducer
