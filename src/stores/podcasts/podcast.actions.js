import ModelListActions from '../actions/model-list-actions'
import { Modules } from '../app.modules'
import Podcast from '../../model/podcast.model'

class PodcastActions extends ModelListActions {
  $collectionId = 'podcasts'
  $module = Modules.PODCASTS

  $convertDocToData(documentSnapshot) {
    return Podcast.fromDocumentSnapshot(documentSnapshot)
  }
}

const podcastActions = new PodcastActions()

export const fetchPodcasts = podcastActions.fetch
export const storePodcast = podcastActions.store
export const postPodcast = podcastActions.post
export const deletePodcast = podcastActions.delete
