import AsyncActions from '../actions/async-actions'
import { Modules } from '../app.modules'
import { uploadFile as upload } from '../../services/file.service'
import logService from '../../services/log.service'
import firebase from '../../services/firebase.service'

class FileActions extends AsyncActions {
  $module = Modules.FILE

  store = (file, path = 'uploads') => dispatch => {
    let url
    return this.$pessimistic(
      upload(file, path).then(),
      dispatch,
      logService.alertError,
      res => url = res
    ).then(() => url)
  }

  delete = url => dispatch => {
    dispatch(this.request)
    return this.$pessimistic(
      firebase.storage().refFromURL(url).delete(),
      dispatch,
      logService.alertError
    )
  }
}

const fileActions = new FileActions()

export const uploadFile = fileActions.store
export const deleteFile = fileActions.delete
