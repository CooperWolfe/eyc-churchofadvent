import Reducer from '../reducers/reducer'
import { Modules } from '../app.modules'

class FileReducer extends Reducer {
  $module = Modules.FILE
}

const fileReducer = new FileReducer()

export default fileReducer
