import { Modules } from './app.modules'
import authReducer from './auth/auth.reducer'
import fileReducer from './file/file.reducer'
import instagramReducer from './instagram/instagram.reducer'
import youthMinistersReducer from './youth-ministers/youthMinisters.reducer'
import eventReducer from './event/event.reducer'
import podcastReducer from './podcasts/podcast.reducer'
import musicReducer from './music/music.reducer'
import videoReducer from './videos/videos.reducer'
import formsReducer from './forms/forms.reducer'
import slidesReducer from './slides/slides.reducer'

const initialState = {
  [Modules.AUTH]: authReducer.getInitialState(),
  [Modules.EVENTS]: eventReducer.getInitialState(),
  [Modules.FILE]: fileReducer.getInitialState(),
  [Modules.FORMS]: formsReducer.getInitialState(),
  [Modules.INSTAGRAM]: instagramReducer.getInitialState(),
  [Modules.MUSIC]: musicReducer.getInitialState(),
  [Modules.PODCASTS]: podcastReducer.getInitialState(),
  [Modules.SLIDES]: slidesReducer.getInitialState(),
  [Modules.VIDEOS]: videoReducer.getInitialState(),
  [Modules.YOUTH_MINISTERS]: youthMinistersReducer.getInitialState(),
}

export default function reducer(state = initialState, action) {
  switch (action.module) {
    case Modules.AUTH: return authReducer.reduce(state, action)
    case Modules.EVENTS: return eventReducer.reduce(state, action)
    case Modules.FILE: return fileReducer.reduce(state, action)
    case Modules.FORMS: return formsReducer.reduce(state, action)
    case Modules.INSTAGRAM: return instagramReducer.reduce(state, action)
    case Modules.MUSIC: return musicReducer.reduce(state, action)
    case Modules.PODCASTS: return podcastReducer.reduce(state, action)
    case Modules.SLIDES: return slidesReducer.reduce(state, action)
    case Modules.VIDEOS: return videoReducer.reduce(state, action)
    case Modules.YOUTH_MINISTERS: return youthMinistersReducer.reduce(state, action)
    default: return state
  }
}
