import ModelListReducer from '../reducers/model-list-reducer'
import { Modules } from '../app.modules'

class VideosReducer extends ModelListReducer {
  $module = Modules.VIDEOS
}

const videoReducer = new VideosReducer()

export default videoReducer
