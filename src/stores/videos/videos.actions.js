import ModelListActions from '../actions/model-list-actions'
import { Modules } from '../app.modules'
import Video from '../../model/video.model'

class VideosActions extends ModelListActions {
  $collectionId = 'videos'
  $module = Modules.VIDEOS

  $convertDocToData(documentSnapshot) {
    return Video.fromDocumentSnapshot(documentSnapshot)
  }
}

const videoActions = new VideosActions()

export const fetchVideos = videoActions.fetch
export const storeVideo = videoActions.store
export const postVideos = videoActions.post
export const deleteVideo = videoActions.delete
