import ModelListActions from '../actions/model-list-actions'
import { Modules } from '../app.modules'
import Music from '../../model/music.model'

class MusicActions extends ModelListActions {
  $collectionId = 'music'
  $module = Modules.MUSIC

  $convertDocToData(documentSnapshot) {
    return Music.fromDocumentSnapshot(documentSnapshot)
  }
}

const musicActions = new MusicActions()

export const fetchMusic = musicActions.fetch
export const storeMusic = musicActions.store
export const postMusic = musicActions.post
export const deleteMusic = musicActions.delete
