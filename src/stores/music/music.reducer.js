import ModelListReducer from '../reducers/model-list-reducer'
import { Modules } from '../app.modules'

class MusicReducer extends ModelListReducer {
  $module = Modules.MUSIC
}

const musicReducer = new MusicReducer()

export default musicReducer
