/**
 * An object describing the subscription to an observable
 */
class Subscription {

  /** @private Observable */ observable
  /** @private function */ observer

  /**
   * Creates a Subscription with a unique identifier
   */
  constructor(observable, observer) {
    this.observable = observable
    this.observer = observer
  }

  /**
   * Unsubscribes the subscription from its observable
   */
  unsubscribe() {
    this.observable.deregister(this.observer)
  }
}

export default Subscription
