import Subscription from './subscription'

class Observable {

  /** @private */ subject
  /** @private function[] */ observers

  /**
   * Creates an observable observing the subject
   * @param subject - what is being observed
   */
  constructor(subject) {
    this.subject = subject === undefined
      ? null
      : subject
    this.observers = []
  }

  /**
   * Registers a callback to be done when the subject changes;
   * Remember to unsubscribe
   * @param {function} callback - what to do when
   * @returns {Subscription} - the subscription to subject changes
   */
  register(callback) {
    // validate input
    if (!callback || typeof callback !== 'function')
      throw new TypeError('callback must be a function')

    // Go ahead and give it something to work with
    callback(this.subject)

    // Add the observer
    this.observers.push(callback)
    return new Subscription(this, callback)
  }

  /**
   * For internal use only. Deregisters a subscription by removing its observer
   * @param {function} observer - the observer to deregister
   */
  deregister(observer) {
    // validate input
    if (!observer || typeof observer !== 'function')
      throw new TypeError('Observable.deregister: parameter observer must be a function')
    if (!this.observers.includes(observer))
      throw new Error('Cannot deregister nonexistent observer')

    // Deregister observer
    const index = this.observers.indexOf(observer)
    this.observers = [
      ...this.observers.slice(0, index),
      ...this.observers.slice(index + 1)
    ]
  }

  /**
   * Pushes a change to the subject and notifies observers
   * @param newValue - the new value to notify observers of
   */
  pushChange(newValue) {
    this.subject = newValue
    this.notify()
  }

  /**
   * Notify observers of a new change
   */
  notify() {
    for (let i = 0; i < this.observers.length; ++i) {
      this.observers[i](this.subject)
    }
  }
}

export default Observable
