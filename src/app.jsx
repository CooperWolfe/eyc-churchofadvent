import React, { Component } from 'react'
import { ToastContainer } from 'react-toastify'
import Content from './components/content'
import Footer from './components/footer'
import Header from './components/header'
import HeaderForm from './components/header-form'
import Navbar from './components/navbar'
import { connect } from 'react-redux'
import { Modules } from './stores/app.modules'

const mapStateToProps = state => ({
  authenticated: state[Modules.AUTH].authenticated
})
const mapDispatchToProps = dispatch => ({})

class App extends Component {
  render() {
    const { authenticated } = this.props

    return (
      <div className='app'>
        <ToastContainer position='top-center'/>
        <Navbar breakpoint='md' style={{ zIndex: 99 }} />
        { authenticated ? <HeaderForm/> : <Header/> }
        <Content />
        <Footer />
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
