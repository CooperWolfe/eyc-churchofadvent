function getIdFromLongVersion(url) {
  const match = url.match(/www\.youtube\.com\/watch\?v=(.*)$/)
  if (!match) return null
  return match[1]
}
function getIdFromShortVersion(url) {
  const match = url.match(/youtu\.be\/(.*)$/)
  if (!match) return null
  return match[1]
}
function getIdFromEmbedVersion(url) {
  const match = url.match(/www\.youtube\.com\/embed\/(.*)$/)
  if (!match) return null
  return match[1]
}
function getId(url) {
  return getIdFromLongVersion(url) || getIdFromShortVersion(url) || getIdFromEmbedVersion(url)
}

export function getThumbnailUrl(videoUrl, version = 'default') {
  const id = getId(videoUrl)
  return `https://img.youtube.com/vi/${id}/${version}.jpg`
}

export function isYoutubeUrl(videoUrl) {
  return !!getId(videoUrl)
}

export function getEmbedUrl(videoUrl) {
  const id = getId(videoUrl)
  return id ? `https://www.youtube.com/embed/${id}` : null
}
