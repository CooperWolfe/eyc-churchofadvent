export function toPhone(numbers) {
  if (numbers.length < 7) return null
  if (numbers.length === 7) {
    const first3 = numbers.substr(0, 3)
    const last4 = numbers.substr(3)
    return `${first3}-${last4}`
  }
  if (numbers.length < 10) return null
  if (numbers.length === 10) {
    const first3 = numbers.substr(0, 3)
    return `(${first3}) ${toPhone(numbers.substr(3))}`
  }

  const countryCode = numbers.substr(0, numbers.length - 10)
  return `${countryCode} ${toPhone(numbers.substr(numbers.length - 10))}`
}

export function toDashCase(str) {
  return str.toLowerCase()
    .replace(/\s/g, '-')
    .replace(/\W/g, '-')
    .replace(/-{2,}/g, '-')
    .match(/\w.*\w/)[0]
}

export function toYesOrNo(bool) {
  return bool ? 'Yes' : 'No'
}

export function toEmailString(obj) {
  return Object.keys(obj)
    .map(key => `${key}: ${typeof obj[key] === 'boolean' ? toYesOrNo(obj[key]) : obj[key]}`)
    .join('\r\n')
}
