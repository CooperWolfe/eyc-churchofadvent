export function toSmallTimeString(date) {
  if (!date || !date.getTime || isNaN(date.getTime())) {
    return null
  }

  let hours = date.getHours()
  let half = 'AM'
  if (hours > 12) {
    hours -= 12
    half = 'PM'
  }
  const minutes = date.getMinutes().toString().padStart(2, '0')

  return `${hours}:${minutes} ${half}`
}

export function today() {
  const now = new Date()
  return new Date(now.getFullYear(), now.getMonth(), now.getDate())
}

export const dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
export const shortDayNames = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat']
export const monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
export const shortMonthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
